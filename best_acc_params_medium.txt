adyghe	acccuracy:	94.7	levenshtein:	0.06	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
albanian	acccuracy:	75.7	levenshtein:	0.67	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
arabic	acccuracy:	33.8	levenshtein:	1.94	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
armenian	acccuracy:	87.5	levenshtein:	0.21	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
asturian	acccuracy:	90.7	levenshtein:	0.2	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
azeri	acccuracy:	87.0	levenshtein:	0.24	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
bashkir	acccuracy:	93.1	levenshtein:	0.15	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
basque	acccuracy:	31.2	levenshtein:	1.72	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
belarusian	acccuracy:	56.0	levenshtein:	1.34	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
bengali	acccuracy:	97.0	levenshtein:	0.08	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
breton	acccuracy:	84.0	levenshtein:	0.3	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
bulgarian	acccuracy:	81.2	levenshtein:	0.35	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
catalan	acccuracy:	81.4	levenshtein:	0.3	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
classical-syriac	acccuracy:	98.0	levenshtein:	0.02	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
cornish	acccuracy:	56.0	levenshtein:	0.8	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
crimean-tatar	acccuracy:	100.0	levenshtein:	0.0	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
czech	acccuracy:	80.1	levenshtein:	0.43	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
danish	acccuracy:	79.3	levenshtein:	0.29	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
dutch	acccuracy:	79.3	levenshtein:	0.33	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
english	acccuracy:	92.4	levenshtein:	0.14	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
estonian	acccuracy:	61.1	levenshtein:	0.75	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
faroese	acccuracy:	52.1	levenshtein:	0.89	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
finnish	acccuracy:	37.1	levenshtein:	1.29	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
french	acccuracy:	70.4	levenshtein:	0.56	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
friulian	acccuracy:	75.0	levenshtein:	0.29	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
galician	acccuracy:	86.7	levenshtein:	0.26	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
georgian	acccuracy:	92.4	levenshtein:	0.16	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
german	acccuracy:	76.0	levenshtein:	0.77	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
greek	acccuracy:	59.3	levenshtein:	1.02	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
greenlandic	acccuracy:	84.0	levenshtein:	0.18	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
haida	acccuracy:	25.0	levenshtein:	1.88	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
hebrew	acccuracy:	77.3	levenshtein:	0.31	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
hindi	acccuracy:	95.4	levenshtein:	0.06	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
hungarian	acccuracy:	54.5	levenshtein:	0.85	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
icelandic	acccuracy:	55.7	levenshtein:	0.79	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
ingrian	acccuracy:	56.0	levenshtein:	0.6	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
irish	acccuracy:	57.8	levenshtein:	1.29	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
italian	acccuracy:	86.0	levenshtein:	0.24	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
kabardian	acccuracy:	100.0	levenshtein:	0.0	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
kannada	acccuracy:	92.0	levenshtein:	0.15	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
karelian	acccuracy:	96.0	levenshtein:	0.04	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
kashubian	acccuracy:	92.0	levenshtein:	0.1	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
kazakh	acccuracy:	94.0	levenshtein:	0.1	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
khakas	acccuracy:	100.0	levenshtein:	0.0	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
khaling	acccuracy:	70.2	levenshtein:	0.56	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
kurmanji	acccuracy:	89.7	levenshtein:	0.16	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
ladin	acccuracy:	80.0	levenshtein:	0.29	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
latin	acccuracy:	19.7	levenshtein:	2.06	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
latvian	acccuracy:	87.6	levenshtein:	0.24	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
lithuanian	acccuracy:	41.5	levenshtein:	1.15	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
livonian	acccuracy:	65.0	levenshtein:	0.83	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
lower-sorbian	acccuracy:	80.2	levenshtein:	0.37	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
macedonian	acccuracy:	87.9	levenshtein:	0.17	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
maltese	acccuracy:	88.0	levenshtein:	0.19	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
mapudungun	acccuracy:	100.0	levenshtein:	0.0	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
middle-french	acccuracy:	90.4	levenshtein:	0.17	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
middle-high-german	acccuracy:	98.0	levenshtein:	0.02	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
middle-low-german	acccuracy:	98.0	levenshtein:	0.06	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
murrinhpatha	acccuracy:	90.0	levenshtein:	0.24	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
navajo	acccuracy:	23.5	levenshtein:	2.8	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
neapolitan	acccuracy:	49.0	levenshtein:	0.71	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
norman	acccuracy:	74.0	levenshtein:	0.54	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
north-frisian	acccuracy:	84.0	levenshtein:	0.42	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
northern-sami	acccuracy:	32.2	levenshtein:	1.63	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
norwegian-bokmaal	acccuracy:	79.1	levenshtein:	0.32	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
norwegian-nynorsk	acccuracy:	55.7	levenshtein:	0.82	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
occitan	acccuracy:	85.0	levenshtein:	0.19	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
old-armenian	acccuracy:	65.5	levenshtein:	0.69	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
old-church-slavonic	acccuracy:	89.0	levenshtein:	0.19	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
old-english	acccuracy:	47.2	levenshtein:	0.96	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
old-french	acccuracy:	61.9	levenshtein:	0.81	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
old-irish	acccuracy:	18.0	levenshtein:	2.64	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
old-saxon	acccuracy:	68.7	levenshtein:	0.54	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
pashto	acccuracy:	81.0	levenshtein:	0.32	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
persian	acccuracy:	84.2	levenshtein:	0.36	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
polish	acccuracy:	77.2	levenshtein:	0.52	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
portuguese	acccuracy:	85.7	levenshtein:	0.22	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
quechua	acccuracy:	75.0	levenshtein:	0.44	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
romanian	acccuracy:	71.4	levenshtein:	0.79	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
russian	acccuracy:	79.4	levenshtein:	0.58	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
sanskrit	acccuracy:	80.5	levenshtein:	0.35	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
scottish-gaelic	acccuracy:	88.0	levenshtein:	0.18	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	1337
serbo-croatian	acccuracy:	72.3	levenshtein:	0.6	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
slovak	acccuracy:	71.9	levenshtein:	0.48	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
slovene	acccuracy:	62.5	levenshtein:	0.76	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
sorani	acccuracy:	52.8	levenshtein:	0.77	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
spanish	acccuracy:	90.3	levenshtein:	0.17	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
swahili	acccuracy:	97.0	levenshtein:	0.06	hidden-size	128	embedding-size	8	layer-size	1	patches	True	enhancer	0	dropout	0	seed	42
swedish	acccuracy:	74.2	levenshtein:	0.48	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	95793
tatar	acccuracy:	95.0	levenshtein:	0.06	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
tibetan	acccuracy:	62.0	levenshtein:	0.7	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
turkish	acccuracy:	69.6	levenshtein:	0.69	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
turkmen	acccuracy:	96.0	levenshtein:	0.06	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
ukrainian	acccuracy:	73.9	levenshtein:	0.46	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
urdu	acccuracy:	96.3	levenshtein:	0.05	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	538
uzbek	acccuracy:	99.0	levenshtein:	0.01	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337
venetian	acccuracy:	94.6	levenshtein:	0.08	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	95793
votic	acccuracy:	47.0	levenshtein:	0.77	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
welsh	acccuracy:	73.0	levenshtein:	0.46	hidden-size	128	embedding-size	8	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
west-frisian	acccuracy:	95.0	levenshtein:	0.13	hidden-size	128	embedding-size	16	layer-size	1	patches	True	enhancer	0	dropout	0	seed	538
yiddish	acccuracy:	96.0	levenshtein:	0.05	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	42
zulu	acccuracy:	84.7	levenshtein:	0.36	hidden-size	128	embedding-size	16	layer-size	1	patches	False	enhancer	0	dropout	0	seed	1337