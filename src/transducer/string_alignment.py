GAP_CHAR = '#'


def align(a: str, b: str, patches=None):
    """
    Align two input words by filling up alignment spaces with GAP_CHAR.
    For examples, see the implementations of the concrete alignment strategies.
    
    In particular, all result strings in the result tuple are guaranteed to have the same length.

    :param a:       First string
    :param b:       Second string
    :param patches: Patch dictionary to consider while aligning (depending on metrics used)

    :return: Tuple of aligned strings
    """
    if patches is None:
        patches = []

    return align_levenshtein(a, b, patches)


def align_levenshtein(a: str, b: str, patches):
    """
    Align two strings by plain Levenshtein metric plus an extra constraint
    Two symbols x, y are considered equal (cost 0) if there is a patch contained in "patches" that transforms x into y

    The alignment with the smallest edit distance is returned.
    If there is a tie between several alignments, the first one that was found originally is chosen.

    align_levenshtein('think', 'thought') --> ('think##', 'thought')
    align_levenshtein('foobar', 'foobazbar') --> ('foo###bar', 'foobazbar')

    :param a:       First string
    :param b:       Second string
    :param patches: Patch dictionary to consider for additional constraint

    :return: The aligned string tuple
    """
    costs = {}
    pointers = {}

    for i in range(len(a) + 1):
        if i > 0:
            pointers[i, 0] = (i - 1, 0)

        costs[i, 0] = i

    for j in range(len(b) + 1):
        if j > 0:
            pointers[0, j] = (0, j - 1)

        costs[0, j] = j

    for i in range(1, len(a) + 1):
        for j in range(1, len(b) + 1):
            if a[i - 1] == b[j - 1] or any(map(lambda v: (a[i - 1], b[j - 1]) in v, patches)):
                costs[i, j] = costs[i - 1, j - 1]
                pointers[i, j] = (i - 1, j - 1)
            else:
                min_parent_cost = min(costs[i - 1, j], costs[i, j - 1], costs[i - 1, j - 1])
                costs[i, j] = min_parent_cost + 1

                for x in range(2):
                    for y in range(2):
                        if (i, j) not in pointers and (x > 0 or y > 0) and costs[i - x, j - y] == min_parent_cost:
                            pointers[i, j] = (i - x, j - y)

    align_gold = (a, b)
    alignment = [''] * len(align_gold)

    c = tuple(map(len, align_gold))

    while c != (0, 0):
        previous = tuple(c)  # clone to avoid reference pointer issues
        c = pointers[c]

        alignment = [x + (GAP_CHAR if previous[i] == c[i] else align_gold[i][c[i]]) for i, x in enumerate(alignment)]

    return tuple(map(lambda r: r[::-1], alignment))

print(align('foobar', 'foobazbar'))