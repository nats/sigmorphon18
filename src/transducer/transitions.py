from transducer.string_alignment import GAP_CHAR

ACTION_CP = '_'
ACTION_MV = '>'
ACTION_EOW = '$'


class TransducerState:
    """
    Finite state transducer working on 'immutable' states in a chain. 
    """

    def __init__(self, base_word, output='', pointer=0, parent=None, prev_action=None, transitions = '', ll = 0.0, raw_nn_out=None):
        self.base_word = base_word
        self.output = output
        self.transitions = transitions
        self.pointer = pointer
        self.raw_nn_out = raw_nn_out
        self.ll = ll # log likelyhood
        self.parent = parent
        self.prev_action = prev_action

    def __repr__(self):
        return "{State baseword:%s output:%s pointer:%s prev_action:%s}" % (self.base_word, self.output, self.pointer, self.prev_action)

    def move(self, action, ll=0.0, raw_nn_out=None):
        # next_pointer = self.pointer + int(self.pointer < (len(self.base_word) - 1))
        next_pointer = self.pointer + 1 if 0 <= self.pointer < (len(self.base_word) - 1) else -1

        return TransducerState(
            self.base_word,
            self.output,
            next_pointer,
            self, ACTION_MV,
            self.transitions + action,
            ll,
            raw_nn_out
        )

    def copy(self, action, ll=0.0, raw_nn_out=None):
        # prevent copying the same value over and over
        copied_val = self.base_word[self.pointer] if self.prev_action != ACTION_CP and self.pointer != -1 else ''

        return TransducerState(
            self.base_word,
            self.output + copied_val,
            self.pointer,
            self, ACTION_CP,
            self.transitions + action,
            ll,
            raw_nn_out
        )

    def emit(self, symbol, ll=0.0, raw_nn_out=None):
        char = symbol if self.pointer != -1 or len(self.output) == 0 or self.output[-1] != symbol else ''
        return TransducerState(
            self.base_word,
            self.output + char,
            self.pointer,
            self, symbol,
            self.transitions + symbol,
            ll,
            raw_nn_out
        )

    def patch(self, patch_class, action, ll=0.0, raw_nn_out=None):
        patch_dict = {start: end for start, end in patch_class}
        current_char = self.base_word[self.pointer] if self.pointer != -1 else ''

        return TransducerState(
            self.base_word,
            self.output + patch_dict.get(current_char, current_char),
            self.pointer,
            self, '#',
            self.transitions + action,
            ll,
            raw_nn_out
        )

    def end(self, ll=0.0, raw_nn_out=None):
        return TransducerState(
            self.base_word,
            self.output,
            self.pointer,
            self, ACTION_EOW,
            self.transitions + ACTION_EOW,
            ll,
            raw_nn_out
        )
    
    def finished(self):
        """ :returns: true if this transition is finished, i.e. an end-of-word action was applied """
        return self.prev_action == ACTION_EOW
    
    def score(self):
        """ :returns: the log likelihood of this state / path """
        return self.ll

    def apply(self, action: str, ll=0.0, raw_nn_out=None, patches=None):
        """
        Applies the action to this state and creating a new state.
            :param action: Action to apply
            :param ll: log likelihood of the new state (summed path probability)
            :param raw_nn_out: Internal neural network data
            :param patches: List of possible patch actions
            :returns: new transducer state
        """
        if patches is None:
            patches = []

        if action == ACTION_MV:
            return self.move(action, ll, raw_nn_out)
        elif action == ACTION_CP:
            return self.copy(action, ll, raw_nn_out)
        elif action.isdigit():
            index = int(action)

            patch_class = patches[index] if index < len(patches) else []
            return self.patch(patch_class, action, ll, raw_nn_out)
        elif action == ACTION_EOW:
            return self.end(ll, raw_nn_out)
        else:
            return self.emit(action, ll, raw_nn_out)

    @classmethod
    def apply_all(cls, base_word, action_sequence, patches=None):
        base_state = cls(base_word)

        patch_buffer = ''

        for a in action_sequence:
            if a == ACTION_EOW:
                return base_state
            elif a.isdigit():
                patch_buffer += a
            else:
                if len(patch_buffer) > 0:
                    base_state = base_state.apply(patch_buffer, patches)
                    patch_buffer = ''

                base_state = base_state.apply(a)

        return base_state


def generate_transitions(input_alignment, output_alignment, patches_list=None):
    """
    Write the gold standard transactions for an input alignment (:param input_alignment) and output alignment (:param output_alignment).

    :param input_alignment Alignment of input string
    :param output_alignment Alignment of output string
    :param patches_list: A list of tuples that represent transformations grouped within one patch

    :return String of transitions
    """
    if len(input_alignment) != len(output_alignment):
        exit("Error: input and output don't have the same length!")

    if patches_list is None:
        patch_dict = {}
    else:
        patch_dict = {pair: i for i, pairs in enumerate(patches_list) for pair in pairs}

    transitions = ''

    for c_in, c_out in zip(input_alignment, output_alignment):
        if c_in == GAP_CHAR:
            transitions += c_out

        elif c_out == GAP_CHAR:
            transitions += ACTION_MV

        elif c_in == c_out:
            transitions += ACTION_CP + ACTION_MV

        elif (c_in, c_out) in patch_dict:
            transitions += str(patch_dict[c_in, c_out]) + ACTION_MV

        elif c_in != c_out:
            transitions += c_out + ACTION_MV

    return transitions.rstrip(ACTION_MV) + ACTION_EOW


def apply_transitions(input_word, transitions, patches_list=None):
    """
    :param input_word: input word to apply the transitions to
    :param transitions: transitions to apply to the input word
    :param patches_list: optional patch classes to detect
    :return: output after transitions have been applied to the input word
    """
    return TransducerState.apply_all(input_word, transitions, patches_list).output


if __name__ == '__main__':
    dummy_patch = [[('a', 'ä'), ('ä', 'a')]]

    actions = generate_transitions('graben', 'gräbst', dummy_patch)

    print(actions)
    print(apply_transitions('graben', actions, dummy_patch))
    print(TransducerState.apply_all('graben', actions, dummy_patch).output)
