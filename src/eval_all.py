#! /usr/bin/env python3

import argparse
import os.path
import evalm
from project_base import LANGUAGES, create_network, language_training, language_dev, output_path, enhanced_data, language_test, best_params_path
from parser.single_set import features_from_file
from networks.base import DataSet
from extract_hyperparams import get_best_params_for_lang

SEEDS = [42, 1337, 95793, 538, 4003046131]

def run_network(network, train_data_path, dev_data_path, test_data_path, train_result, dev_result, test_result, model_file=None):
    train_data, feature_key = features_from_file(train_data_path)
    dev_data, _ = features_from_file(dev_data_path, feature_key)
    test_data, _ = features_from_file(test_data_path, feature_key)
    network.prepare_data(train_data, dev_data, test_data)

    if model_file:
        network.load_model(model_file)
    else:
        network.run_training()
    
    network.evaluate_all(train_result, train_data, DataSet.train)
    network.evaluate_all(dev_result, dev_data, DataSet.dev)
    network.evaluate_all(test_result, test_data, DataSet.test)

def evaluate(guess, gold):
    eval_result = evalm.main(gold, guess, 1)

    with open(guess + "-eval.txt", "w+") as eval_file:
        eval_file.write(eval_result)

def iterate_seeds(lang, size, iterations, network, train_data_path, dev_data_path, test_data_path, params={}):
    train_result_base = output_path('evaluation', lang, size, file='trainresult')
    dev_result_base = output_path('evaluation', lang, size, file='devresult')
    test_result_base = output_path('evaluation', lang, size, file='testresult')
    for seed in SEEDS[0:iterations]:
        train_result = train_result_base + "-seed-" + str(seed) + ".txt"
        dev_result = dev_result_base + "-seed-" + str(seed) + ".txt"
        test_result = test_result_base + "-seed-" + str(seed) + ".txt"

        params["seed"] = seed
        net_instance = create_network(network, debug_print=True, **params)
        run_network(net_instance, train_data_path, dev_data_path, test_data_path, train_result, dev_result, test_result)
        evaluate(train_result, train_data_path)
        evaluate(dev_result, dev_data_path)

def use_best_params(best_params, model_folder, lang, size, network, train_data_path, dev_data_path, test_data_path, iterations):
    train_result = output_path('evaluation', lang, size, file='trainresult-best.txt')
    dev_result = output_path('evaluation', lang, size, file='devresult-best.txt')
    test_result = output_path('evaluation', lang, size, file='testresult-best.txt')
    enhancer = best_params.pop("enhancer", False)
    if enhancer:
        train_data_path = enhanced_data(lang, size, enhancer)
       
    if iterations > 1:
        iterate_seeds(lang, size, iterations, network, train_data_path, dev_data_path, test_data_path, best_params)
    else:
        net_instance = create_network(network, debug_print=True, **best_params)
        # try to load existing model or train using known best seed
        model_file = None
        if model_folder:
            model_file = output_path(model_folder, lang, size, file='best_levenshtein.model')
            model_file = model_file if os.path.isfile(model_file) else None

        run_network(net_instance, train_data_path, dev_data_path, test_data_path, train_result, dev_result, test_result, model_file)
        if model_file is None:
            model_file = output_path(model_folder, lang, size, file='best_levenshtein.model')
            net_instance.dump(model_file)
        evaluate(train_result, train_data_path)
        evaluate(dev_result, dev_data_path)

def main(network, single_lang, all_langs, resume, use_low, use_medium, use_high, use_enhanced, params_file, model_folder, iterations='1'):
    languages = []

    iters = int(iterations)

    if single_lang:
        languages = [single_lang]
    elif all_langs:
        languages = LANGUAGES
    elif resume:
        index = LANGUAGES.index(resume)
        languages = LANGUAGES[index:]

    for lang in languages:
        best_params = get_best_params_for_lang(best_params_path(params_file), lang) if params_file else None
        for cond, size in zip([use_low, use_medium, use_high], ['low', 'medium', 'high']):
            if not cond:
                continue
            print("\t".join(["Running: ", lang, str(size), "Enhanced: " + str(use_enhanced)]))
            if use_enhanced:
                train_data_path = enhanced_data(lang, size)
            else:
                train_data_path = language_training(lang, size)
            if not os.path.isfile(train_data_path):
                continue 
            dev_data_path = language_dev(lang)
            test_data_path = language_test(lang)
            if best_params:
                use_best_params(best_params, model_folder, lang, size, network, train_data_path, dev_data_path, test_data_path, iters)
            else:
                iterate_seeds(lang, size, iters, network, train_data_path, dev_data_path, test_data_path)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Trains and evaluates a network on a single or all languages')

    parser.add_argument('-n', '--network', dest='network', default='fst')
    parser.add_argument('-s', '--single', dest='single_lang', const='middle-high-german', nargs='?')
    parser.add_argument('-a', '--all', dest='all_langs', const=True, default=False, action='store_const')
    parser.add_argument('-r', '--resume', dest='resume')
    parser.add_argument('-l', '--low', dest='use_low', default=False, action='store_const', const=True)
    parser.add_argument('-m', '--medium', dest='use_medium', default=False, action='store_const', const=True)
    parser.add_argument('-g', '--high', dest='use_high', default=False, action='store_const', const=True)
    parser.add_argument('-e', '--enhanced', dest='use_enhanced', default=False, action='store_const', const=True)
    parser.add_argument('-i', '--iterations', dest='iterations', default='1')
    parser.add_argument('-p', '--params', dest='params_file', help="File with best parameters for each language, created by extract_hyperparams.py from hyperparameter output folder")
    parser.add_argument('-f', '--modelfolder', dest='model_folder', default="hyperparameter", help="Foldername in output-folder to look for models")

    args = parser.parse_args()

    main(**vars(args))
