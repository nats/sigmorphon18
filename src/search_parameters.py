#! /usr/bin/env python3

import evalm
import project_base

import datetime
from argparse import ArgumentParser
from itertools import product
from random import shuffle
from parser.single_set import features_from_file
from networks.base import DataSet
from extract_hyperparams import get_best_params_for_lang
import os
from enhancer.enhance_data_to_file import write_random_data


HIDDEN_SIZES = [128]
EMBEDDING_SIZES = [16]
LAYER_SIZES = [1]
PATCHES = [True, False]
ENHANCER = [0, 1, 5]
DROPOUTS = [0]
SEEDS = [42, 1337, 95793, 538, 84346, 9886, 92631, 33643, 8109, 2402]

PARAMS_NAMES = ["hidden-size", "embedding-size", "layer-size", "patches", "enhancer", "dropout", "seed"]

OUTPUT_MODULE = 'hyperparameter'


def get_all_combinations(discrete_params):
    """
    Returns the product of all discrete params making sure dropout only used when layers > 1
    :param discrete_params: discrete parameters to be tested
    :return:
    """
    prod_sequence = product(*discrete_params)
    # remove illegal combinations of single layer and dropout > 0
    return [x for x in prod_sequence if (x[2] > 1 and x[5] > 0) or (x[2] == 1 and x[5] == 0)]


def network_run(train_data, dev_data, model, hidden_size, embedding_size, layers, dropout, patch, seed):
    network = project_base.create_network(model, hidden_size=hidden_size, debug_print=True, seed=seed,
                                          embedding_size=embedding_size, layer_size=layers, dropout=dropout,
                                          patches=patch)

    network.prepare_data(train_data, dev_data)
    network.run_training()
    return network


def reduce_parameters(params_file, lang, lang_env, hidden_sizes, embedding_sizes, patches, enhancer):
    """
    Reduce the parameter space by taking the results of previous runs into account.
    Idea: Make a broad search on the low set, refine parameters for each language in the medium set
    Hidden and embedding sizes are therefore reduced to greater (max +1) or equal the previous best
    Patches True/False is copied from previous best result for sets larger low
    Enhancer is always deactivated when dataset != low
    """

    params = get_best_params_for_lang(project_base.best_params_path(params_file),
                                      lang) if params_file is not None else None

    if params is None:
        enhancer = [0] if lang_env != "low" else enhancer
        return hidden_sizes, embedding_sizes, patches, enhancer

    hs = params["hidden_size"]
    ih = hidden_sizes.index(hs)
    lh = len(hidden_sizes)

    if lang_env != "high":
        es = params["embedding_size"]
        ie = embedding_sizes.index(es)
        le = len(embedding_sizes)
        embedding_sizes[ie:min(ie + 2, le)]
    else:
        embedding_sizes = [params["embedding_size"]]

    if lang_env != "low":
        enhancer = [0]
        patches = [params["patches"]]
    else:
        enhancer_value = params["enhancer"]
        if type(enhancer_value) is int:
            enhancer = [enhancer_value]

    return hidden_sizes[ih:min(ih + 2, lh)], embedding_sizes, patches, enhancer


def test_parameters(lang, lang_env, model, params_file,
                    hidden_sizes=HIDDEN_SIZES, embedding_sizes=EMBEDDING_SIZES,
                    layer_sizes=LAYER_SIZES, dropouts=DROPOUTS, patches=PATCHES, enhancer=ENHANCER,
                    seeds=SEEDS, max_tests=0):
    """
    :param model: which network model to use (currently only supports FST)
    :param hidden_sizes: list of hidden sizes
    :param embedding_sizes: list of embedding sizes
    :param layer_sizes: list of layer sizes
    :param dropouts: list from which the dropout will be selected
    :param patches: list of patch options (maximum True and False)
    :param enhancer: list of Enhancer options (maximum True or False)
    :param seeds: list of Random Seeds
    :param max_tests: how many tests should be done per iteration. 0=tests for all parameter combinations
    :return: best accuracy and its  levenshtein, model and parameters
    """
    assert (max_tests >= 0), 'max_tests must be at least 0'

    # hidden_sizes, embedding_sizes, patches, enhancer = reduce_parameters(params_file, lang, lang_env, hidden_sizes, embedding_sizes, patches, enhancer)
    # seeds are not included here, because every choosen paramter configuration has to be tested with all seeds

    if params_file is not None:
        params = get_best_params_for_lang(project_base.best_params_path(params_file),
                                          lang)
        hidden_sizes = [params["hidden_size"]]
        embedding_sizes = [params["embedding_size"]]
        # seeds = [params["seed"]]

    discrete_params = [hidden_sizes, embedding_sizes, layer_sizes, patches, enhancer, dropouts]
    print(discrete_params)

    product_list = get_all_combinations(discrete_params)
    shuffle(product_list)  # randomize list
    print(product_list)
    len_prod = len(product_list)

    # initialize best values as impossible values:
    best_accuracy = -1
    best_levenshtein = 1000000

    best_accuracy_parameters = []
    best_levenshtein_parameters = []

    # unrestricted: test ALL parameter combinations; else: test max_tests parameter combinations
    if max_tests == 0:
        max_tests = len_prod

    lang_output = [OUTPUT_MODULE, lang, lang_env]
    best_accuracy_model = project_base.output_path(*lang_output, file='best_accuracy.model')
    best_levenshtein_model = project_base.output_path(*lang_output, file='best_levenshtein.model')

    data_cache = {}

    # test one parameter combination in shuffled list after the other
    for i in range(0, min(max_tests, len_prod)):
        selected_parameters = product_list[i]  # pick current parameters

        for seed in seeds:
            curr_parameters = (*selected_parameters, seed)

            param_dict = dict(zip(PARAMS_NAMES, curr_parameters))

            parameter_strings = [str(k) + '-' + str(v) for k, v in param_dict.items()]
            parameter_title = '_'.join(parameter_strings)

            filename_deveval = project_base.output_path(*lang_output, file=parameter_title + '.deveval')
            if os.path.isfile(filename_deveval):
                # already computed this one in a previous run
                print("Skipped:", parameter_title)
                continue

            # use enhanced data here if parameter permutation says so
            curr_enhanced_parameter = curr_parameters[4]
            if curr_enhanced_parameter > 0 and lang_env == 'low':  # TODO this is dirty
                lang_data = project_base.enhanced_data(lang, lang_env, curr_enhanced_parameter)
                if not os.path.isfile(lang_data):
                    print("\t".join(["Creating enhanced data for:", lang, lang_env, str(curr_enhanced_parameter)]))
                    original_data = project_base.language_training(lang, lang_env)
                    name = project_base.enhanced_data_filename(lang, lang_env)
                    write_random_data(name=name, file=original_data, num=curr_enhanced_parameter,
                                      output_folder=project_base.enhanced_data_folder(curr_enhanced_parameter))
            else:
                lang_data = project_base.language_training(lang, lang_env)

            network_parameters = [curr_parameters[i] for i in (0, 1, 2, 5, 3, 6)]

            train_data, feature_key = data_cache.get(lang_data, features_from_file(lang_data))
            data_cache[lang_data] = train_data, feature_key
            dev_data, _ = features_from_file(project_base.language_dev(lang), feature_key)

            print("Running", lang, "on", lang_env, "with params", parameter_title)
            curr_network = network_run(train_data, dev_data, model, *network_parameters)

            train_result = project_base.output_path(*lang_output, file=parameter_title + '.trainresult')
            curr_network.evaluate_all(train_result, train_data, DataSet.train)
            dev_result = project_base.output_path(*lang_output, file=parameter_title + '.devresult')
            curr_network.evaluate_all(dev_result, dev_data, DataSet.dev)

            eval_train = evalm.main(lang_data, train_result, 1)
            eval_dev = evalm.main(project_base.language_dev(lang), dev_result, 1)

            extract_val = [val for i, val in enumerate(eval_dev.split('\t')) if i in (2, 4)]
            accuracy, levenshtein = map(float, extract_val)

            today = datetime.datetime.now()  # log current results
            output_text = "{d}\tacccuracy:\t{a}\tlevenshtein:\t{lev}\tparameters:\t{p}".format(d=today,
                                                                                               a=accuracy,
                                                                                               lev=levenshtein,
                                                                                               p=param_dict)

            # logs the results
            with open(project_base.output_path(*lang_output, file='parameters_search.log'), "a+") as log:
                log.write(output_text + "\n")

            with open(project_base.output_path(*lang_output, file=parameter_title + '.traineval'), 'w+') as f:
                f.write(eval_train)

            with open(project_base.output_path(*lang_output, file=parameter_title + '.deveval'), 'w+') as f:
                f.write(eval_dev)

            best_accuracy, best_accuracy_parameters = best_params(True, best_accuracy, accuracy,
                                                                  best_accuracy_parameters, curr_parameters,
                                                                  curr_network, best_accuracy_model,
                                                                  'best_accuracy_parameters_search.log',
                                                                  lang_output, output_text)

            best_levenshtein, best_levenshtein_parameters = best_params(False, best_levenshtein, levenshtein,
                                                                        best_levenshtein_parameters, curr_parameters,
                                                                        curr_network, best_levenshtein_model,
                                                                        'best_levenshtein_parameters_search.log',
                                                                        lang_output, output_text)

    # for better output
    result_names = ["Accuracy", "Levenshtein-Distance", "Model (acc)", "Model (lev)", "best Parameters (acc)",
                    "best Parameters (lev)"]
    return best_accuracy, best_levenshtein, best_accuracy_model, best_levenshtein_model, best_accuracy_parameters, best_levenshtein_parameters, result_names


def best_params(higher, best_val, new_val, best_param, current_param, current_network, modelfile, logname, lang_output,
                output_text):
    check = new_val > best_val if higher else new_val < best_val
    if check:
        best_val = new_val

        current_network.dump(modelfile)
        best_param = current_param

        # logs the best results, i.e. improvements
        with open(project_base.output_path(*lang_output, file=logname), "a+") as log:
            log.write(output_text + "\n")
    return best_val, best_param


def main(lang, lang_env, model, params_file):
    best_results = test_parameters(lang, lang_env, model, params_file, max_tests=0)
    # TODO make this work with any output format
    print("Best results:" + str(list(zip(best_results[-1], [str(best_results[0]), str(best_results[1]),
                                                            (list(zip(PARAMS_NAMES, best_results[5])))]))))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('-l', '--language', help='The language to train on', dest='lang', default='swedish')
    parser.add_argument('-e', '--lang-environment', help='The environment (high, medium, low) of the language to use',
                        dest='lang_env', choices=project_base.LANG_ENVIRONMENTS, default='low')
    parser.add_argument('-m', '--model-path', help='The model to use for training', dest='model', default='fst')
    parser.add_argument('-p', '--params', dest='params_file',
                        help="File with best parameters for each language, created by extract_hyperparams.py from result folder of this script")

    args = parser.parse_args()

    main(**vars(args))
    print("Done")
