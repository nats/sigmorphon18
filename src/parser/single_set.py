from itertools import starmap
from parser.util import read_data, extract_features, join_line_features, FEATURE_DELIMITER


def features_from_file(file, inflection_key=None):
    word_table = read_data(file)

    inflection_set = {f for features in map(extract_features, word_table) for f in features}
    sorted_inflections = inflection_key or sorted(inflection_set)

    vector_list = fill_vector_list(word_table, sorted_inflections)

    return vector_list, sorted_inflections


def fill_vector_list(word_table, feature_set):
    one_hot = [[int(f in features) for f in feature_set] for features in map(extract_features, word_table)]
    return list(starmap(join_line_features, zip(word_table, one_hot)))


def inflections_from_vector(vector, keys):
    used_inflections = [k for i, k in enumerate(keys) if vector[i] == 1]
    return FEATURE_DELIMITER.join(used_inflections)
