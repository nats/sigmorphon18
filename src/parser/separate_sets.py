from itertools import zip_longest, starmap

from parser.util import extract_features, read_data, join_line_features, FEATURE_DELIMITER


def features_from_file(file, inflection_key=None):
    word_table = read_data(file)

    inflection_sets = [{p for p in part_set if p} for part_set in zip_longest(*map(extract_features, word_table))]
    inflection_list = inflection_key or [list(sorted(s)) for s in inflection_sets]

    vector_list = fill_vector_list(word_table, inflection_list)

    return vector_list, inflection_list


def fill_vector_list(word_table, feature_groups):
    feature_set = list(map(extract_features, word_table))

    dense = [[gr.index(ft[i]) + 1 if i < len(ft) else 0 for i, gr in enumerate(feature_groups)] for ft in feature_set]
    sparse = [[int(p - 1 == j) for i, p in enumerate(d) for j in range(len(feature_groups[i]))] for d in dense]

    return list(starmap(join_line_features, zip(word_table, dense, sparse)))


def inflections_from_dense_vector(vector, keys):
    used_inflections = [keys[i][k - 1] for i, k in enumerate(vector) if k > 0]
    return FEATURE_DELIMITER.join(used_inflections)


def inflections_from_sparse_vector(vector, keys):
    groupable = [i for i in vector]  # copy values to avoid reference/pointer leaks
    grouped_vector = [[groupable.pop(0) for _ in range(i)] for i in map(len, keys)]

    group_indices = [group.index(1) for group in grouped_vector if 1 in group]

    return inflections_from_dense_vector(group_indices, keys)
