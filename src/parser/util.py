import codecs
import string

from itertools import starmap

FEATURE_DELIMITER = ';'


def read_data(txtfile):
    with codecs.open(txtfile, encoding='utf-8') as fp:
        return [line.rstrip('\n').split('\t') for line in fp]

def extract_features(line):
    return line[-1].split(FEATURE_DELIMITER)


def join_line_features(line, *features):
    return line + list(features)

