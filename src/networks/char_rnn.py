from typing import Iterable, List
import numpy as np
import torch
from torch.autograd import Variable

from networks.base import AbstractNetwork
from networks.model import BahdanauAttentionDecoderRNN

BATCH_SIZE = 1
START_OF_WORD_INDEX = 0
END_OF_WORD_INDEX = 0

class CharNetwork(AbstractNetwork):
    """
    seq2seq network working on characters instead of words, with learned soft attention
    """

    def __init__(self, hidden_size=64, embedding_size=8, layer_size=1, dropout=0.0, patch=False, seed=42, epochs=100, debug_print=True):
        super().__init__(BATCH_SIZE, hidden_size, embedding_size, layer_size, dropout, seed, debug_print=True)

    def get_pre_indexed_word(self):
        """ :returns: the special actions requiring fix indices """
        return '|' # start/end of word

    def setup_decoder(self, output_alphabet_count: int, feature_count: int):
        """
        Constructs the decoder with soft attention.
            :param output_alphabet_count: Number of possible actions
            :param feature_count: Size of the feature tensor (must be identical for train, dev & test!)
            :returns: decoder model
        """
        return BahdanauAttentionDecoderRNN(self.hidden_size, self.embedding_size, output_alphabet_count,
                                           feature_size=feature_count)

    def backprop(self, enc_optim, dec_optim, loss):
        """
        Applies backpropagation on the loss and changes the weights according to the optimizers.
            :param enc_optim: Encoder optimizer
            :param dec_optim: Decoder optimizer
            :param loss: Tensor/Variable to be used for backpropagation
        """
        loss.backward()
        CLIP = 4.0
        torch.nn.utils.clip_grad_norm_(self.encoder.parameters(), CLIP)
        torch.nn.utils.clip_grad_norm_(self.decoder.parameters(), CLIP)
        enc_optim.step()
        dec_optim.step()

    def predict(self, input_words: List[str], input_tensor: torch.Tensor, input_lenghts: List[int], input_features: torch.Tensor,
                target_tensor: torch.Tensor = None, target_transitions : List[str] =[None], encoder_optimizer: torch.optim.Optimizer = None,
                decoder_optimizer: torch.optim.Optimizer = None, criterion: torch.nn.Module = None, max_length: int = 100, training: bool = True):
        """
        Predicts the inflected word form from lemma and features.
            :param input_words: List of lemma strings
            :param input_tensor: Input lemma tensor
            :param input_lenghts: Length of the words (for packed tensor)
            :param input_features: Inflection feature tensor
            :param target_tensor: Target tensor with index representation of the target transition sequence
            :param param target_transitions: List of target transition sequences as strings
            :param encoder_optimizer: Optimizer for the encoder
            :param decoder_optimizer: Optimizer for the decoder
            :param criterion: Loss criterion to be used, i.e. NLL or Hinge
            :param max_length: Maximum predicted action sequence length
            :param training: Must be True during training, False when evaluating
            :returns: List of inflected output words as string
            :returns: List of inflected output words as string
        """
        
        teacher_forcing = False
        target_length = max_length
        if training:
            # Zero gradients of both optimizers
            encoder_optimizer.zero_grad()
            decoder_optimizer.zero_grad()
        else:
            self.encoder.eval()
            self.decoder.eval()

        # Run words through encoder
        encoder_outputs, encoder_hidden = self.encoder(input_tensor, input_lenghts)

        # Prepare input and output variables
        decoder_input = Variable(torch.zeros(self.batch_size, dtype=torch.long))
        # decoder_input[:] = STARTSTART_OF_WORD_INDEX_OF_WORD not needed, START_OF_WORD_INDEX = 0

        # Use last hidden state from encoder (forward only) to start decoder
        decoder_hidden = encoder_hidden[0:1, :, :]

        predicted_transitions: List[List[int]] = [[]]*self.batch_size
        total_loss = 0
        finished_batches = set()

        for i in range(target_length):
            decoder_output, decoder_hidden = self.decoder(decoder_input, input_features, decoder_hidden,
                                                          encoder_outputs)

            topv, topi = decoder_output.data.topk(1)

            predictions: List[int] = [topi[i][0].item() for i in range(self.batch_size)]
            loss_batches = 0

            for b in range(self.batch_size):
                if b in finished_batches:
                    continue
                prediction = predictions[b]
                if training and target_tensor[i].item() != prediction:
                    loss_batches += criterion(decoder_output[b:b+1], target_tensor[i])
                
                if prediction == END_OF_WORD_INDEX:
                    finished_batches.add(b)
                    continue
                
                predicted_transitions[b].append(prediction)

            if training and loss_batches != 0:
                total_loss += loss_batches
                if teacher_forcing:
                        prediction = target_tensor[i].item()
                else:
                    self.backprop(encoder_optimizer, decoder_optimizer, loss_batches)
                    break

            if len(finished_batches) == self.batch_size:
                break
            
            # Chosen char is next input
            decoder_input = Variable(torch.LongTensor(predictions))


        if training:
            if teacher_forcing and total_loss != 0:
                self.backprop(encoder_optimizer, decoder_optimizer, total_loss)

        decoded_chars_batch = ["".join(map(lambda ind: self.out_chars.index2char[ind], transition_indices)) for transition_indices in predicted_transitions] 
        return decoded_chars_batch, decoded_chars_batch
