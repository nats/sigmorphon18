import math
from typing import List

import torch
import torch.nn as nn
import torch.nn.functional as F


class EncoderRNN(nn.Module):
    """
    Encoder module with learned char embeddings and bi-directional GRU. 
    Based on https://github.com/AuCson/PyTorch-Batch-Attention-Seq2seq
    """
    def __init__(self, input_size: int, embed_size:int , hidden_size: int, n_layers: int = 1, dropout: float = 0):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.embedding = nn.Embedding(input_size, embed_size)
        self.gru = nn.GRU(embed_size, hidden_size, n_layers, dropout=dropout, bidirectional=True)

    def forward(self, input_tensor: torch.Tensor, input_lengths: List[int], hidden: torch.Tensor = None):
        """
        Computes the forward operation.
            :param input_tensor: Tensor of shape (max(word_length),batch_size), sorted decreasingly by lengths (for packing)
            :param input_lengths: List of input word lengths
            :param hidden: Initial state of GRU, Tensor of shape (layers*2,batc size, hidden size)
            :returns:
                GRU outputs in shape (max(word_length),batch size,hidden_size)
                last hidden state of the GRU
        """
        embedded = self.embedding(input_tensor)
        packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, input_lengths)
        outputs, hidden = self.gru(packed, hidden)

        # unpack
        outputs, output_lengths = torch.nn.utils.rnn.pad_packed_sequence(outputs)

        # Sum bidirectional outputs
        outputs = outputs[:, :, :self.hidden_size] + outputs[:, :, self.hidden_size:]

        return outputs, hidden


class HardAttentionDecoderRNN(nn.Module):
    """ GRU decoder with fix context. Works on single characters, requires an outer loop for decoding a word """
    def __init__(self, hidden_size: int, embed_size: int, output_size: int, n_layers=1, feature_size=0, dropout=0):
        super(HardAttentionDecoderRNN, self).__init__()

        # Define layers
        self.embedding = nn.Embedding(output_size, embed_size)
        self.gru = nn.GRU(hidden_size + embed_size + feature_size, hidden_size, n_layers, dropout=dropout)
        self.out = nn.Linear(hidden_size, output_size)

    def forward(self, char_input, features_input, last_hidden, encoder_output):
        """
        Computes the forward operation with the given tensors / variables.
            :param char_input: char input for current time step, in shape (batch size)
            :param features_input: inflection features (n-hot)
            :param last_hidden: last hidden state of the decoder, in shape (layers, batch size, hidden size)
            :param encoder_output: encoder outputs in shape (1, batch size, hidden size)
            :returns:
                decoder output for a single input char
                current hidden state
        """

        # Get the embedding of the current input char (last output char)
        char_embedded = self.embedding(char_input).unsqueeze(0)
        context = encoder_output.unsqueeze(0)
        # Combine embedded input char, given context and features
        rnn_input = torch.cat((char_embedded, context, features_input), 2)
        output, hidden = self.gru(rnn_input, last_hidden)
        output = output.squeeze(0)
        linear_transform = self.out(output)
        output = F.log_softmax(linear_transform, dim=1)
        return output, hidden


class SoftAttention(nn.Module):
    """
    Bahdanau soft attention module
    based on https://github.com/AuCson/PyTorch-Batch-Attention-Seq2seq
    """
    def __init__(self, hidden_size):
        super(SoftAttention, self).__init__()
        self.attn = nn.Linear(hidden_size * 2, hidden_size)
        self.v = nn.Parameter(torch.rand(hidden_size))
        stdv = 1. / math.sqrt(self.v.size(0))
        self.v.data.normal_(mean=0, std=stdv)

    def forward(self, hidden, encoder_outputs):
        """
        Computes the forward operation
            :param hidden: previous hidden state of the decoder, in shape (layers*directions, batch size, hidden size)
            :param encoder_outputs: outputs from encoder, in shape (word length, batc size, hidden size)
            :returns: attention energies in shape (batch size, word length)
        """
        max_len = encoder_outputs.size(0)
        H = hidden.repeat(max_len, 1, 1).transpose(0, 1)
        encoder_outputs = encoder_outputs.transpose(0, 1)
        attn_energies = self.score(H, encoder_outputs)
        return F.softmax(attn_energies, dim=1).unsqueeze(1)

    def score(self, hidden, encoder_outputs):
        """ :returns: the attention score per character in the word """
        energy = F.tanh(self.attn(torch.cat([hidden, encoder_outputs], 2)))  # [B*T*2H]->[B*T*H]
        energy = energy.transpose(2, 1)  # [B*H*T]
        v = self.v.repeat(encoder_outputs.data.shape[0], 1).unsqueeze(1)  # [B*1*H]
        energy = torch.bmm(v, energy)  # [B*1*T]
        return energy.squeeze(1)  # [B*T]


class BahdanauAttentionDecoderRNN(nn.Module):
    """
    GRU decoder with soft attention
    based on from https://github.com/AuCson/PyTorch-Batch-Attention-Seq2seq
    """
    def __init__(self, hidden_size: int, embed_size: int, output_size: int, n_layers=1, dropout_p=0, feature_size=0):
        super(BahdanauAttentionDecoderRNN, self).__init__()
        # Define layers
        self.embedding = nn.Embedding(output_size, embed_size)
        self.attn = SoftAttention(hidden_size)
        self.gru = nn.GRU(hidden_size + embed_size + feature_size, hidden_size, n_layers, dropout=dropout_p)
        self.out = nn.Linear(hidden_size, output_size)

    def forward(self, char_input, features_input, last_hidden, encoder_outputs):
        """
        Computes the forward operation with the given tensors / variables.
            :param char_input: char input for current time step, in shape (batch size)
            :param features_input: inflection features (n-hot)
            :param last_hidden: last hidden state of the decoder, in shape (layers, batch size, hidden size)
            :param encoder_output: encoder outputs in shape (1, batch size, hidden size)
            :returns:
                decoder output for a single input char
                current hidden state
        """
        # Get the embedding of the current input char (last output char)
        char_embedded = self.embedding(char_input).view(1, char_input.size(0), -1)
        # Calculate attention weights and apply to encoder outputs
        attn_weights = self.attn(last_hidden[-1], encoder_outputs)

        context = attn_weights.bmm(encoder_outputs.transpose(0, 1))  # (B,1,V)
        context = context.transpose(0, 1)  # (1,B,V)

        # Combine embedded input char, attended context and features
        rnn_input = torch.cat((char_embedded, context, features_input), 2)

        output, hidden = self.gru(rnn_input, last_hidden)
        output = output.squeeze(0)  # (1,B,V)->(B,V)
        linear_transform = self.out(output)
        output = F.log_softmax(linear_transform, dim=1)
        return output, hidden
