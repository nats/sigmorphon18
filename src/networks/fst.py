from itertools import chain
from typing import Iterable, List
import math

import numpy as np
import torch
from torch.autograd import Variable

from networks.base import AbstractNetwork
from networks.model import HardAttentionDecoderRNN
from optical_characters.patches import generate_patches
from transducer.string_alignment import align
from transducer.transitions import generate_transitions, ACTION_CP, ACTION_MV, ACTION_EOW, TransducerState

ACTION_CP_IND = 0
ACTION_MV_IND = 1
ACTION_EOW_IND = 2
BATCH_SIZE = 1

class FstNetwork(AbstractNetwork):
    """
    Finite State Transducer working on the input sequence by asking the network what action to take.
    Supports beam-search decoding.
    """

    def __init__(self, hidden_size=64, embedding_size=8, layer_size=1, dropout=0.0,
                 patches=True, seed=42, epochs=100, lr=0.005, debug_print=False):
        super().__init__(BATCH_SIZE, hidden_size, embedding_size, layer_size, dropout, seed, epochs, lr, debug_print)
        self.patch = patches
        self.patches = []


    def transform_target(self, word_data) -> List[str]:
        """
        Converts the target words into the target action sequence
            :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
            :returns: List of target transition sequences
        """
        words_lemma, words_gold, _, _ = zip(*word_data)
        alignments = [align(lemma, gold) for lemma, gold in zip(words_lemma, words_gold)]
        return [generate_transitions(base, flex, self.patches) for base, flex in alignments]


    def prepare_data(self, word_data_train, word_data_dev, word_data_test=None):
        """
        Prepare data by generating patches if needed. Calls super for actual data loading.
            :param word_data_train: Training set 4-tuple of lemma, target, feature text, feature tensor
            :param word_data_dev: Development set 4-tuple of lemma, target, feature text, feature tensor
            :param word_data_test: Test set 4-tuple of lemma, target, feature text, feature tensor (may be None)
        """

        if self.patch:
            words_lemma, words_gold, _, _ = zip(*word_data_train)
            chars = {c for word in chain(words_lemma, words_gold) for c in word}
            self.patches = generate_patches(chars)
            self.print_if(self.patches)

        super().prepare_data(word_data_train, word_data_dev, word_data_test)


    def get_pre_indexed_word(self):
        """ :returns: the special actions requiring fix indices """
        return ACTION_CP + ACTION_MV + ACTION_EOW


    def setup_decoder(self, output_alphabet_count: int, feature_count: int):
        """
        Constructs the decoder with hard monotonic attention.
            :param output_alphabet_count: Number of possible actions
                   (sum of all writable characters, special actions, patches)
            :param feature_count: Size of the feature tensor (must be identical for train, dev & test!)
            :returns: decoder model
        """
        return HardAttentionDecoderRNN(self.hidden_size, self.embedding_size, output_alphabet_count,
                                       self.layers, feature_count, self.dropout)


    def backprop(self, enc_optim, dec_optim, loss):
        """
        Applies backpropagation on the loss and changes the weights according to the optimizers.
            :param enc_optim: Encoder optimizer
            :param dec_optim: Decoder optimizer
            :param loss: Tensor/Variable to be used for backpropagation
        """
        loss.backward()
        # clipping currently disabled
        # CLIP = 4.0
        # torch.nn.utils.clip_grad_norm_(self.encoder.parameters(), CLIP)
        # torch.nn.utils.clip_grad_norm_(self.decoder.parameters(), CLIP)
        enc_optim.step()
        dec_optim.step()


    def find_gold_beam(self, beam_states: List[TransducerState], target_transition: str) -> int:
        """
        Finds the correct path within the beam.
            :param beam_states: Transducer states currently in the beam
            :param target_transition: correct transition sequence
            :returns: correct path index or -1 if not found
        """

        # a 'dynamic' oracle would work on the inflected word, not the transition sequence
        # target_transitions should then contain the target word
        # prefix matching would need to changed to some scoring criteria
 
        for i, state in enumerate(beam_states):
            if target_transition.startswith(state.transitions):
                return i
        return -1


    def apply_prediction_on_state(self, state: TransducerState, predicted_index: int,
                                  decoder_hidden: torch.Tensor, decoder_output: torch.Tensor) -> TransducerState:
        """
        Applies the predicted action on the given transducer state,
        creating a new state with the decoder tensors.
            :param state: Transducer state the action should be applied upon
            :param predicted_index: Index of the predicted action
            :param decoder_hidden: Hidden state of the decoder after predicting the action
            :param decoder_output: Raw decoder output from which the max likely action was extracted
            :returns: Next transducer state
        """
        predicted_char: str = self.out_chars.index2char[predicted_index]    

        # Chosen char is next input
        decoder_input = torch.LongTensor([predicted_index])
        decoder_result = decoder_input, decoder_hidden

        # sum the log likelyhood over the path
        path_ll = state.ll + decoder_output[0, predicted_index]
        return state.apply(predicted_char, path_ll, decoder_result, self.patches)


    def beam_search(self, beam_size: int, target_tensor: torch.Tensor, target_length: int, input_features: torch.Tensor,
                    encoder_outputs: torch.Tensor, target_transition: str, initial_state: TransducerState, training: bool = True):
        """
        Performs beam-search decoding.
            :param beam_size: Number of paths to keep, linear increase in runtime
            :param target_tensor: Target tensor with index representation of the target transition sequence
            :param target_length: Length of the target transition sequence (needed, because tensor is packed)
            :param input_features: Inflection feature tensor
            :param encoder_outputs: Encoder output tensor for the entire lemma
            :param target_transition: Target transition sequence as a string
            :param initial_state: Initial transducer state to start decoding
            :param training: Must be False when predicting / evaluating (when target is unknown)
            :returns: List of current states in the beam once decoding has stopped
            :returns: Index of the correct (when training) or best path (otherwise)
            :returns: Path length (number of predicted actions)
        """

        current_states: List[TransducerState] = [initial_state]
        gold_state = initial_state
        counter = 0
        gold_beam_index = 0

        while (counter < target_length and not current_states[0].finished() and gold_beam_index >= 0):
            next_states = []
            for state in current_states:
                decoder_input, decoder_hidden = state.raw_nn_out
                decoder_output, decoder_hidden = self.decoder(decoder_input, input_features, decoder_hidden,
                                                            encoder_outputs[state.pointer, :, :])

                topv, topi = decoder_output.topk(beam_size)    
                for b in range(beam_size):
                    predicted_index: int =  topi[0, b].item()
                    next_states.append(self.apply_prediction_on_state(state, predicted_index, decoder_hidden, decoder_output))

            next_states.sort(key=lambda a: a.ll, reverse=True)

            if training:
                gold_beam_index = self.find_gold_beam(next_states, target_transition)
                if gold_beam_index < 0:
                    # manually add next gold beam based on current gold beam
                    decoder_input, decoder_hidden = gold_state.raw_nn_out
                    gold_state = self.apply_prediction_on_state(gold_state, target_tensor[counter, 0].item(), decoder_hidden, decoder_output)
                elif gold_beam_index >= beam_size:
                    # correct path is in beam, but will fall out
                    gold_beam_index = -1
                else:
                    # the gold path stays in beam
                    gold_state = next_states[gold_beam_index]

            current_states = next_states[0:beam_size]
            counter += 1
        
        if training and gold_beam_index < 0:
            # add correct path to end of beam states if its not already in there
            current_states.append(gold_state)
            gold_beam_index = beam_size

        return current_states, gold_beam_index, counter


    def beam_loss(self, beam_states: List[TransducerState], gold_beam_index: int, path_length: int) -> torch.Tensor:
        """
        Computes a locally normalized loss on the correct path in the beam.
        See D. Andor et. al., 2016: Globally Normalized Transition-Based Neural Networks Equation (6) and (4)
            :param beam_states: List of final transducer states after decoding
            :param gold_beam_index: Index of the correct path
            :param path_length: Length of the decoded paths (number of actions)
            :returns: Loss tensor
        """
        # global optimization, training not working well
        # log_likelyhoods = torch.stack([b.ll for b in beam_states])
        # return - beam_states[gold_beam_index].ll + torch.log(torch.sum(torch.exp(log_likelyhoods)))

        # local optimization, normalized by path length for more consistent loss magnitude
        return - beam_states[gold_beam_index].ll / math.log(path_length + 1.0)

        # punish best (but still wrong) path, training not working well
        # return beam_states[0].ll - beam_states[gold_beam_index].ll


    def predict(self, input_words: List[str], input_tensor: torch.Tensor, input_lenghts: List[int], input_features: torch.Tensor,
                target_tensor: torch.Tensor = None, target_transitions : List[str] =[None], encoder_optimizer: torch.optim.Optimizer = None,
                decoder_optimizer: torch.optim.Optimizer = None, criterion: torch.nn.Module = None, max_length: int = 100, training: bool = True):
        """
        Predicts the inflected word form from lemma and features. This method assumes a batch size of 1.
            :param input_words: Input lemma string
            :param input_tensor: Input lemma tensor
            :param input_lenghts: Length of the word (for packed tensor)
            :param input_features: Inflection feature tensor
            :param target_tensor: Target tensor with index representation of the target transition sequence
            :param param target_transitions: Target transition sequence as a string
            :param encoder_optimizer: Optimizer for the encoder
            :param decoder_optimizer: Optimizer for the decoder
            :param criterion: Not used as this class has its custom loss function
            :param max_length: Maximum predicted action sequence length
            :param training: Must be True during training, False when evaluating
            :returns: inflected output word string
            :returns: final transition sequence
        """

        target_length = max_length
        # must be smaller than output alphabet size
        beam_size = min(1, self.out_chars.char_count)

        if training:
            # Zero gradients of both optimizers
            encoder_optimizer.zero_grad()
            decoder_optimizer.zero_grad()
        else:
            self.encoder.eval()
            self.decoder.eval()

        # Run words through encoder
        encoder_outputs, encoder_hidden = self.encoder(input_tensor, input_lenghts)

        # Prepare initial decoder input
        decoder_input_start = Variable(torch.zeros(self.batch_size, dtype=torch.long))
        # decoder_input[:] = START_OF_WORD # not needed, START_OF_WORD = 0

        # Use last hidden state from encoder (forward only) to start decoder
        decoder_hidden_start = encoder_hidden[0:1, :, :]

        # char/string-based transition system
        baseword: str = input_words[0]
        decoder_start = decoder_input_start, decoder_hidden_start
        initial_state: TransducerState = TransducerState(baseword, raw_nn_out=decoder_start)

        # run beam search decoding
        beam_states, gold_beam_index, steps = self.beam_search(beam_size, target_tensor, target_length, input_features,
                                                        encoder_outputs, target_transitions[0], initial_state, training)
        
        # compute loss once correct path not in beam at top position
        if training:
            if gold_beam_index > 0:
                loss = self.beam_loss(beam_states, gold_beam_index, steps)
                self.backprop(encoder_optimizer, decoder_optimizer, loss)
            elif gold_beam_index < 0:
                print("Error! No gold beam available for loss calculation.")

        return [beam_states[0].output], [beam_states[0].transitions]
