import codecs
import math
import os
import time
import random
from typing import Iterable, List
from functools import reduce
from tempfile import NamedTemporaryFile
from enum import Enum

import numpy as np
import torch
import torch.nn as nn
from torch import optim
from torch.autograd import Variable

from networks.model import EncoderRNN

EMPTY_CHAR = '_'
EMPTY_CHAR_INDEX = 0

class DataSet(Enum):
    """ the 3 types of data sets """
    train = 1
    dev = 2
    test = 3

class AbstractNetwork:
    """ Contains basic functionality shared by the different networks. """

    def __init__(self, batch_size, hidden_size=64, embedding_size=16, layer_size=1,
                 dropout=0.0, seed=42, epochs=100, lr=0.001, debug_print=False):
        self.embedding_size = embedding_size
        self.hidden_size = hidden_size
        self.layers = layer_size
        self.dropout = dropout
        self.batch_size = batch_size
        self.epochs = epochs
        self.lr = lr

        self.debug_print = debug_print

        self.in_chars = Char2Tensor()
        self.out_chars = Char2Tensor()

        self.max_length_train = 0
        self.max_length_dev = 0
        self.max_length_test = 0

        self.length_diff_abs = 0

        self.encoder = None
        self.decoder = None

        self.input_train_tensor = None
        self.target_train_tensor = None
        self.samples = None

        # better performace as pytorch does not seem to work well on multiple cores
        torch.set_num_threads(1)

        # reproduceable results
        self.seed = seed
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        np.random.seed(seed)
        random.seed(seed)

        # sanitize
        if self.layers < 2:
            self.dropout = 0.0

    def print_if(self, *values, sep=' ', end='\n', file=None, flush=True):
        """ print function that may be enabled or disabled """
        if self.debug_print:
            print(*values, sep=sep, end=end, file=file, flush=flush)

    def get_data_batch(self, i, dataset=DataSet.train):
        """
        Fetches data from the matching data set.
            :param i: Index in the data set
            :param dataset: Which data set to use
            :returns: sample of the data set
        """ 
        if dataset is DataSet.train:
            return self.make_data_batch(self.input_train_tensor, self.input_train_words, self.target_train_tensor, self.target_train_words, self.target_train_transitions, self.words_lengths_train_tensor, self.feature_train_tensor, i, self.batch_size)
        elif dataset is DataSet.dev:
            return self.make_data_batch(self.input_dev_tensor, self.input_dev_words, self.target_dev_tensor, self.target_dev_words, self.target_dev_transitions, self.words_lengths_dev_tensor, self.feature_dev_tensor, i, self.batch_size)
        elif dataset is DataSet.test:
            return self.input_test_tensor[:, i:i + self.batch_size], self.input_test_words[i:i + self.batch_size], None, None, None, self.words_lengths_test_tensor[i:i+self.batch_size].data.tolist(), self.feature_test_tensor[i:i+self.batch_size].unsqueeze(0)
        
    def make_data_batch(self, input_tensor: torch.Tensor, input_words: List[str], target_tensor: torch.Tensor,
                        target_word: List[str], target_transition: List[str], word_len: torch.Tensor,
                        features: torch.Tensor, i: int, batch_size: int):
        """
        Creates a sample wit all required information from the data set.
            :param input_tensor: Input lemma tensor
            :param input_words: Input lemma strings
            :param target_tensor: Target tensor
            :param target_word: Target word string
            :param target_transition: Target transition sequence string
            :param word_len: Input lemma lengths tensor
            :param features: Feature tensor
            :param i: Index
            :param batch_size: Size of the resulting sample
            :returns: 7-tuple containing a batch of lemma tensor, lemma words, target tensor, target words, target sequence, input lengths, feature tensor
        """
        batch_x = input_tensor[:, i:i + batch_size]
        batch_x_words = input_words[i:i + batch_size]
        batch_y = target_tensor[:, i:i + batch_size]
        batch_y_words = target_word[i:i+batch_size]
        batch_y_transitions = target_transition[i:i+batch_size]
        batch_lengths = word_len[i:i + batch_size].data.tolist()
        batch_features = features[i:i + batch_size].unsqueeze(0)

        return batch_x, batch_x_words, batch_y, batch_y_words, batch_y_transitions, batch_lengths, batch_features

    def prepare_data(self, word_data_train, word_data_dev, word_data_test=None):
        """
        Prepare data by generating patches if needed. Calls super for actual data loading.
            :param word_data_train: Training set 4-tuple of lemma, target, feature text, feature tensor
            :param word_data_dev: Development set 4-tuple of lemma, target, feature text, feature tensor
            :param word_data_test: Test set 4-tuple of lemma, target, feature text, feature tensor (may be None)
        """

        self.load_training_input_data(word_data_train)
        self.load_training_output_data(word_data_train)

        self.load_dev_input_data(word_data_dev)
        self.load_dev_output_data(word_data_dev)

        if word_data_test:
            self.load_test_data(word_data_test)

        self.encoder = self.encoder if self.encoder else self.setup_encoder()
        self.decoder = self.decoder if self.decoder else self.setup_decoder(self.out_chars.char_count, self.feature_train_tensor.size(1))

    def load_training_input_data(self, word_data):
        """
        Processes the training data, filling the internal datastructures for inputs.
        :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
        """
        words_lemma, _, _, features = zip(*word_data)
        words_lemma_lengths = [len(word) for word in words_lemma]

        self.words_lengths_train_tensor = torch.LongTensor(words_lemma_lengths)
        self.feature_train_tensor = torch.FloatTensor(features)
        self.in_chars.index_word(EMPTY_CHAR)
        self.in_chars.index_words(words_lemma)
        self.input_train_tensor = self.in_chars.words2tensor_batch(words_lemma)
        self.input_train_words = words_lemma
    
    def load_dev_input_data(self, word_data):
        """
        Processes the development data, filling the internal datastructures for inputs.
        :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
        """
        words_lemma, _, _, features = zip(*word_data)
        words_lemma_lengths = [len(word) for word in words_lemma]
        dev_input_max_length = self.compute_max_length(words_lemma)
        old_length = self.in_chars.max_length

        self.words_lengths_dev_tensor = torch.LongTensor(words_lemma_lengths)
        self.feature_dev_tensor = torch.FloatTensor(features)
        self.in_chars.max_length = dev_input_max_length
        self.input_dev_tensor = self.in_chars.words2tensor_batch(words_lemma)
        self.in_chars.max_length = old_length
        self.input_dev_words = words_lemma
    
    def load_test_data(self, word_data):
        """
        Processes the test data, filling the internal datastructures for inputs.
        :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
        """
        words_lemma, _, features = zip(*word_data)
        words_lemma_lengths = [len(word) for word in words_lemma]
        test_input_max_length = self.compute_max_length(words_lemma)
        old_length = self.in_chars.max_length

        self.words_lengths_test_tensor = torch.LongTensor(words_lemma_lengths)
        self.feature_test_tensor = torch.FloatTensor(features)
        self.in_chars.max_length = test_input_max_length
        self.input_test_tensor = self.in_chars.words2tensor_batch(words_lemma)
        self.in_chars.max_length = old_length
        self.input_test_words = words_lemma

        train_length_rel = (self.out_chars.max_length / self.in_chars.max_length) + 0.25
        dev_length_abs = 10 + abs(self.out_chars.max_length - self.in_chars.max_length)

        self.max_length_test = self.approx_test_output_length(test_input_max_length)

    def load_training_output_data(self, word_data):
        """
        Processes the training data, filling the internal datastructures for targets.
        :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
        """
        pre_index_word = self.get_pre_indexed_word()

        self.target_train_words = AbstractNetwork.transform_target(self, word_data)
        self.target_train_transitions = self.transform_target(word_data)
        target_lengths_train_tensor = torch.LongTensor([len(word) for word in self.target_train_words])
        length_diff_abs = torch.max(target_lengths_train_tensor - self.words_lengths_train_tensor).item()
        self.length_diff_abs = max(self.length_diff_abs, length_diff_abs)

        self.out_chars.index_word(pre_index_word)  # start of word, copy char, end of word
        self.out_chars.index_words(self.target_train_transitions)
        self.max_length_train = self.out_chars.max_length
        self.target_train_tensor = self.out_chars.words2tensor_batch(self.target_train_transitions)
    
    def load_dev_output_data(self, word_data):
        """
        Processes the development data, filling the internal datastructures for targets.
        :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
        """
        self.target_dev_words = AbstractNetwork.transform_target(self, word_data)
        self.target_dev_transitions = self.transform_target(word_data)
        target_lengths_dev_tensor = torch.LongTensor([len(word) for word in self.target_dev_words])
        length_diff_abs = torch.max(target_lengths_dev_tensor - self.words_lengths_dev_tensor).item()
        self.length_diff_abs = max(self.length_diff_abs, length_diff_abs)

        old_length = self.out_chars.max_length
        self.max_length_dev = self.compute_max_length(self.target_dev_transitions)
        self.out_chars.max_length = self.max_length_dev
        self.target_dev_tensor = self.out_chars.words2tensor_batch(self.target_dev_transitions)
        self.out_chars.max_length = old_length

        max_samples_count = 1000
        size = len(self.target_dev_words)
        # sample only x of size to do faster checking (esp. on the low set)
        self.samples = np.random.choice(size, size=min(size, max_samples_count), replace=False)
    
    def compute_max_length(self, words: Iterable[str]) -> int:
        return max(map(lambda w: len(w), words))

    def approx_test_output_length(self, input_length: int) -> int:
        return 2 * input_length + self.length_diff_abs + 3
        
    def setup_encoder(self):
        """ :returns: the default encoder """
        return EncoderRNN(self.in_chars.char_count, self.embedding_size, self.hidden_size, self.layers, self.dropout)

    def setup_decoder(self, output_alphabet_count, feature_count):
        """
        Constructs the decoder
            :param output_alphabet_count: Number of possible actions
            :param feature_count: Size of the feature tensor (must be identical for train, dev & test!)
            :returns: decoder model
        """
        pass

    def transform_target(self, word_data) -> Iterable[str]:
        """
        Allows transformation of the target in derived classes.
            :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
            :returns: List of targets
        """
        # default: just return the gold standard
        _, words_gold, _, _ = zip(*word_data)
        return words_gold

    def get_pre_indexed_word(self) -> str:
        """ :returns: the special actions requiring fix indices """
        pass

    def predict(self, input_words: List[str], input_tensor: torch.Tensor, input_lenghts: List[int], input_features: torch.Tensor,
                target_tensor: torch.Tensor = None, target_transitions : List[str] =[None], encoder_optimizer: torch.optim.Optimizer = None,
                decoder_optimizer: torch.optim.Optimizer = None, criterion: torch.nn.Module = None, max_length: int = 100, training: bool = True):
        """
        Predicts the inflected word form from lemma and features.
            :param input_words: List of lemma strings
            :param input_tensor: Input lemma tensor
            :param input_lenghts: Length of the words (for packed tensor)
            :param input_features: Inflection feature tensor
            :param target_tensor: Target tensor with index representation of the target transition sequence
            :param param target_transitions: List of target transition sequences as strings
            :param encoder_optimizer: Optimizer for the encoder
            :param decoder_optimizer: Optimizer for the decoder
            :param criterion: Loss criterion to be used, i.e. NLL or Hinge
            :param max_length: Maximum predicted action sequence length
            :param training: Must be True during training, False when evaluating
            :returns: List of inflected output words as string
            :returns: List of inflected output words as string
        """
        pass

    def dump(self, file):
        """ Saves the current weights of encoder and decoder to :param file: """
        # TODO should save full state, i.e. parameters, optimizer etc.
        torch.save([self.encoder, self.decoder], file)

    def load_model(self, file):
        """ Loads the current weights of encoder and decoder from :param file: """
        # TODO should load full state, i.e. parameters, optimizer etc.
        self.encoder, self.decoder = torch.load(file)

    def save_checkpoint(self, file):
        """ Saves the current weights of encoder and decoder to :param file: """
        torch.save([self.encoder, self.decoder], file)
    
    def load_checkpoint(self, file):
        """ Loads the current weights of encoder and decoder from :param file: """
        self.encoder, self.decoder = torch.load(file)

    def evaluate_all(self, filename, word_data, dataset):
        """
        Evaluates the entire dataset
            :param filename: Output filename
            :param word_data: 4-Tuple of lemma, target, feature text, feature tensor
            :param dataset: Type of the dataset to evaluate
        """

        max_length = 100
        if dataset is DataSet.train:
            words_lemma, _, features_human, _ = zip(*word_data)
        elif dataset is DataSet.dev:
            words_lemma, _, features_human, _ = zip(*word_data)
        elif dataset is DataSet.test:
            words_lemma, features_human, _ = zip(*word_data)
        else:
            print("Error, unknown dataset")
            return

        with EvalOutputWriter(filename) as writer:
            size = len(word_data)
            for i in range(0, size, self.batch_size):
                self.print_if('\rEvaluating %s of %s' % (i+1, size), end='')
                batch_x, batch_x_words, batch_y, batch_y_words, batch_y_transitions, batch_lengths, batch_features = self.get_data_batch(i, dataset)

                if batch_y_transitions is None:
                    max_length = max((self.approx_test_output_length(length) for length in batch_lengths))
                else:
                    max_length = max(map(lambda x: len(x), batch_y_transitions))

                decoded_chars_batch, additional_info_batch = self.predict(batch_x_words, batch_x, batch_lengths, batch_features, max_length=max_length, training=False)

                lemma_batch = words_lemma[i:i + self.batch_size]
                features_batch = features_human[i:i + self.batch_size]

                for prediction, lemma, feat in zip(decoded_chars_batch, lemma_batch, features_batch):
                    writer.write_entry(lemma, prediction, feat)

        self.print_if('')
        self.print_if("Predictions in file", filename)
    
    def validation_error(self):
        """ :returns: the validation error on the development set"""
        predictions = []
        targets = []
        for i in self.samples:
            batch_x, batch_x_words, batch_y, batch_y_words, batch_y_transitions, batch_lengths, batch_features = self.get_data_batch(i, DataSet.dev)
            prediction_batch, additional_info_batch = self.predict(batch_x_words, batch_x, batch_lengths, batch_features, max_length=self.max_length_dev, training=False)
            predictions.extend(prediction_batch)
            targets.extend(batch_y_words)
        return self.score(predictions, targets)
    
    def score(self, predictions: Iterable[str], targets: Iterable[str]):
        """
        Computes a dissimilarity score based on Levenshtein distance (devided by target length)
            :param predictions: Predicted inflections
            :param targets: Correct inflections
            :returns: averaged dissimilarity score
        """
        from evalm import distance
        total_err = 0.0
        for word, target in zip(predictions, targets):
                total_err += distance(word, target) / len(target)
        return total_err / min(len(predictions), len(targets))


    def run_training(self):
        """
        Trains the network while validating on the dev set.
        Stops training after 15 episodes without validation score improvements.
        """
        
        # Initialize optimizers and criterion
        encoder_optimizer = optim.Adam(self.encoder.parameters(), lr=self.lr, weight_decay=0.001)
        decoder_optimizer = optim.Adam(self.decoder.parameters(), lr=self.lr, weight_decay=0.001)
        criterion = nn.NLLLoss(size_average=True)

        # Configuring training
        data_set_length = self.input_train_tensor.size(1)
        n_epochs = self.epochs

        # Keep track of time elapsed and running averages
        start = time.time()
        loss_total = 0
        print_every = 1

        # for early stopping / dev validation during training
        start_val_epoch = 15 # start validating from epoch x onwards
        start_val_training_loss = 0.1 # start validating once train err is lower than x
        train_loss_reached = False
        stop_epochs_eval = 15 # stop at x epochs without better validation score
        epochs_val_not_better_count = 0
        current_best_val = 1000
        tmp_file = NamedTemporaryFile(buffering=2097152) # 2mb buffer
        train_loss_near_zero = 0.005

        # Begin!
        for epoch in range(1, n_epochs + 1):
            
            predictions = []
            targets = []
            permutation = np.random.permutation(data_set_length)

            for i in permutation:
                # Get training data for this cycle
                batch_x, batch_x_words, batch_y, batch_y_words, batch_y_transitions, batch_lengths, batch_features = self.get_data_batch(i, DataSet.train)

                # Predict in training mode
                prediction_batch, additional_info_batch = self.predict(batch_x_words, batch_x, batch_lengths, batch_features, batch_y, batch_y_transitions, encoder_optimizer,
                                                decoder_optimizer, criterion, self.max_length_train, True)

                # Keep track of the training results
                predictions.extend(prediction_batch)
                targets.extend(batch_y_words)

            loss_epoch = self.score(predictions, targets)
            loss_total += loss_epoch

            train_loss_reached |= loss_epoch < start_val_training_loss

            if epoch == 0:
                continue
           
            validation_error = -1
            if (epoch > start_val_epoch or train_loss_reached):
                validation_error = self.validation_error()
                
                if validation_error > current_best_val or loss_epoch < train_loss_near_zero:
                    # no improvement
                    epochs_val_not_better_count += 1

                    if epochs_val_not_better_count > stop_epochs_eval or loss_epoch < train_loss_near_zero:
                        # stop training and restore best model for evaluation
                        self.load_checkpoint(tmp_file.name)
                        tmp_file.close()
                        self.print_if("Reloaded best model state, val err", current_best_val)
                        break
                elif validation_error < current_best_val:
                    # some improvement, save weights
                    current_best_val = validation_error
                    epochs_val_not_better_count = 0
                    self.save_checkpoint(tmp_file.name)

            if epoch % print_every == 0:
                loss_avg = loss_total / epoch

                print_summary = '%s (%d %d%%) %.4f (avg) %.4f (epoch) %.4f (val-err)' % (
                    time_since(start, epoch / n_epochs), epoch, epoch / n_epochs * 100, loss_avg, loss_epoch, validation_error)
                self.print_if(print_summary)
        

class EvalOutputWriter:
    """ Provides means to write the results in the required format for the official evaluation script. """

    def __init__(self, filename: str):
        """ :param filename: Result filename/path """
        parent_path = os.path.dirname(filename)
        os.makedirs(parent_path, exist_ok=True)

        self.output = codecs.open(filename, "w+", "utf-8", buffering=2097152) # 2mb buffer

    def write_entry(self, lemma: str, prediction: str, features: str):
        """
        Writes a single entry to the file.
            :param lemma: Input lemma
            :param prediction: Predicted inflection
            :param features: Human readable features
        """
        self.output.write(lemma)
        self.output.write('\t')
        self.output.write(prediction)
        self.output.write('\t')
        self.output.write(features)
        self.output.write('\n')

    def close(self):
        """ Manually close the file """
        self.output.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


class Char2Tensor:
    """ Allows to convert words into tensors (index based) and back. """

    def __init__(self) -> None:
        self.char2index = {}
        self.index2char = {}
        self.char_count = 0
        self.max_length = 0

    def index_word(self, word: str) -> None:
        """ Adds any new chars to the dictionary """
        self.max_length = max(self.max_length, len(word))

        for char in word:
            if char not in self.char2index:
                self.char2index[char] = self.char_count
                self.index2char[self.char_count] = char
                self.char_count = self.char_count + 1

    def index_words(self, words: Iterable[str]) -> None:
        """ Adds any new chars to the dictionary """
        for word in words:
            self.index_word(word)

    def word2tensor(self, word: str) -> torch.Tensor:
        """ Transforms a single word to a index tensor """
        tensor = torch.zeros(1, len(word), dtype=torch.long)

        for li, letter in enumerate(word):
            tensor[0][li] = self.char2index.get(letter, EMPTY_CHAR_INDEX)

        return tensor

    def words2tensor_batch(self, words: Iterable[str]) -> torch.Tensor:
        """ Transforms all words at once to index tensor """
        tensor = torch.zeros((self.max_length, len(words)), dtype=torch.long)

        for i, word in enumerate(words):
            for j, char in enumerate(word):
                tensor[j][i] = self.char2index.get(char, EMPTY_CHAR_INDEX)

        return tensor

    def tensor2word(self, tensor: torch.Tensor) -> str:
        """ Transforms a tensor back into a single word """
        word = ""
        for i in range(tensor.size(0)):
            index = tensor[i][0].item()
            word += self.index2char[index]
        return word

    def tensor2words_batch(self, tensor: torch.Tensor, batch_size) -> List[str]:
        """ Transforms a tensor back into a list of words """
        words = [""] * batch_size

        for i in range(0, batch_size):
            for j in range(tensor.size(0)):
                index = tensor[i][j].item()
                words[i] += self.index2char[index]

        return words


def as_minutes(seconds):
    """ :returns: the :param seconds: as a minute+seconds string """
    m = math.floor(seconds / 60)
    seconds -= m * 60

    return '%dm %ds' % (m, seconds)


def time_since(since, percent):
    """ :returns: the elapsed and remaining time """
    now = time.time()
    s = now - since

    es = s / percent
    rs = es - s

    return '%s (- %s)' % (as_minutes(s), as_minutes(rs))
