#! /usr/bin/env python3

import argparse
import numpy as np
import matplotlib.pyplot as plt
import os
from project_base import output_path, OUTPUT, PLOTFOLDER

IND_ACC = 2
IND_LEV = 4
IND_OUT = 8


def parse_log(folder, volume):
    languages = []
    acc_dev = []
    lev_dev = []
    acc_train = []
    lev_train = []
    #    for lang_folder in folder:
    lang_folders = os.listdir(folder)  # get all subfolders (i.e. langauges)
    for lang in lang_folders:
        if os.path.isdir(os.path.join(folder, lang)):
            size_folders = os.listdir(os.path.join(folder, lang))
            for size in size_folders:
                if size.startswith(volume):
                    languages.append("-".join([lang, size]))
                    files = os.listdir((os.path.join(folder, lang, size)))
                    train_eval = ""
                    dev_eval = ""
                    for file in files:
                        # find the training evaluation
                        if file.startswith("train") and file.endswith("eval.txt"):
                            train_eval = file
                        # find the dev evaluation
                        elif file.startswith("dev") and file.endswith("eval.txt"):
                            dev_eval = file

                    with open(os.path.join(folder, lang, size, train_eval)) as te:
                        line = te.readline().strip()
                        tokens = line.split("\t")
                        acc = float(tokens[IND_ACC])
                        lev = float(tokens[IND_LEV])
                        acc_train.append(acc)
                        lev_train.append(lev)

                    with open(os.path.join(folder, lang, size, dev_eval)) as de:
                        line = de.readline().strip()
                        tokens = line.split("\t")
                        acc = float(tokens[IND_ACC])
                        lev = float(tokens[IND_LEV])
                        acc_dev.append(acc)
                        lev_dev.append(lev)

                    # empty fields to make sure that the lists still have the same lengths even if a file is missing
                    if train_eval == "":
                        acc_train.append("")
                    if dev_eval == "":
                        lev_train.append("")
    return languages, np.array(acc_train), np.array(lev_train), np.array(acc_dev), np.array(lev_dev)


def plot(indices, values, title, x_names, filename=""):
    plt.bar(indices, values)
    #    plt.bar(indices, acc_train - accuracy, bottom=accuracy)
    plt.title(title)
    plt.xticks(indices, x_names, rotation='vertical')
    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.40)
    fig.set_size_inches(20.0,20.0)
    fig.canvas.set_window_title(title)
    if not filename == "":
        plt.savefig(filename)
    plt.show()


def get_average(list):
    return (sum(list) / len(list))


def compare_two_folders(folder1, folder2, volume, only_not_zero=False, separate_plots=False):
    languages_p, acc_train_p, lev_train_p, acc_dev_p, lev_dev_p = main(os.path.join(OUTPUT, folder1),
                                                                       separate_plots=separate_plots, volume=volume)
    languages, acc_train, lev_train, acc_dev, lev_dev = main(os.path.join(OUTPUT, folder2),
                                                             separate_plots=separate_plots, volume=volume)

    diffs_acc_train = [b_i - a_i for a_i, b_i in zip(acc_train_p, acc_train)]
    diffs_acc_dev = [b_i - a_i for a_i, b_i in zip(acc_dev_p, acc_dev)]

    diff_acc_list = []

    for i in list(zip(languages, diffs_acc_train, diffs_acc_dev)):
        if ((i[1] != 0 or i[2] != 0) and only_not_zero) or not only_not_zero:
            diff_acc_list.append(i)

    # sort by train diffs
    diff_acc_list = sorted(diff_acc_list, key=lambda diff_acc_train: diff_acc_train[1])
    plot(range(0, len(diff_acc_list)), [x for _, x, _ in diff_acc_list], "Acc Diff Train (sorted by value)",
         [x for x, _, _ in diff_acc_list], "plot_acc_val_train.png")

    # sort by Dev diffs
    diff_list = sorted(diff_acc_list, key=lambda diff_train: diff_train[2])
    plot(range(0, len(diff_acc_list)), [x for _, _, x in diff_list], "Acc Diff Dev (sorted by value)",
         [x for x, _, _ in diff_list], "plot_acc_val_dev.png")

    plot(range(0, len(diffs_acc_train)), diffs_acc_train, "Acc Diff Train (alphabetically sorted)", languages,
         "plot_acc_alph_train.png")
    plot(range(0, len(diffs_acc_dev)), diffs_acc_train, "Acc Diff Dev (alphabetically sorted)", languages,
         "plot_acc_alph_dev.png")

    # Levenshtein

    diffs_lev_train = [b_i - a_i for a_i, b_i in zip(lev_train_p, lev_train)]
    diffs_lev_dev = [b_i - a_i for a_i, b_i in zip(lev_dev_p, lev_dev)]

    diff_lev_list = []

    for i in list(zip(languages, diffs_lev_train, diffs_lev_dev)):
        if ((i[1] != 0 or i[2] != 0) and only_not_zero) or not only_not_zero:
            diff_lev_list.append(i)

    # sort by train diffs
    diff_list = sorted(diff_list, key=lambda diff_train: diff_train[1])
    plot(range(0, len(diff_lev_list)), [x for _, x, _ in diff_lev_list], "Lev Diff Train (sorted by value)",
         [x for x, _, _ in diff_lev_list], "plot_lev_val_train.png")

    # sort by Dev diffs
    diff_list = sorted(diff_list, key=lambda diff_train: diff_train[2])
    plot(range(0, len(diff_lev_list)), [x for _, _, x in diff_lev_list], "Lev Diff Dev (sorted by value)",
         [x for x, _, _ in diff_lev_list], "plot_lev_val_dev.png")

    plot(range(0, len(diffs_lev_train)), diffs_lev_train, "Lev Diff Train (alphabetically sorted)", languages,
         "plot_lev_alph_train.png")
    plot(range(0, len(diffs_lev_dev)), diffs_lev_train, "Lev Diff Dev (alphabetically sorted)", languages,
         "plot_lev_alph_dev.png")


def main(folder, volume="low", separate_plots=True):
    evaluation_folder = folder

    languages, acc_train, lev_train, acc_dev, lev_dev = parse_log(evaluation_folder, volume)

    # plot(languages, acc_train, acc_dev, "acc")
    # plot(languages, lev_dev, lev_train, "lev")

    indices = list(range(0, len(acc_train)))
    if separate_plots:
        plot(indices, acc_train, "acc-train", languages, os.path.join(OUTPUT, folder, "acc-train-low.png"))
        plot(indices, acc_dev, "acc-dev", languages, os.path.join(OUTPUT, folder, "acc-dev-low.png"))
        plot(indices, lev_train, "lev-train", languages, os.path.join(OUTPUT, folder, "lev-train-low.png"))
        plot(indices, lev_dev, "lev-dev", languages, os.path.join(OUTPUT, folder, "lev-dev-low.png"))

    print("\t".join(
        ["Folder:", folder, "Overall Mean Train Accuracy:", str(get_average(acc_train)), "Overall Mean Dev Accuracy:",
         str(get_average(acc_dev)), "Overall Mean Train Lev-Dist.:", str(get_average(lev_train)),
         "Overall Mean Dev Lev-Dist.:", str(get_average(lev_dev))]))
    return languages, acc_train, lev_train, acc_dev, lev_dev


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extracts results from log file and plots them')
    parser.add_argument('-f', '--folder', dest='folder', default=os.path.join(OUTPUT, "evaluation"))
    parser.add_argument('-v', '--volume', dest='volume', default="low")
    args = parser.parse_args()
    main(args.folder, args.volume)
