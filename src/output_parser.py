import os
import re
import argparse
import numpy as np
from itertools import product
from collections import OrderedDict
from statistics import median
from project_base import PROJECT
import matplotlib.pyplot as plt
from scipy.stats import binom_test

from matplotlib import rcParams

rcParams.update({'figure.autolayout': True})
rcParams.update({'font.size': 8})
rcParams.update({'xtick.labelsize': 7})
rcParams.update({'ytick.labelsize': 7})

VOLUME_NAMES = ["low", "medium", "high"]
BASELINE = os.path.join(PROJECT, "baseline_log.txt")

ACC_POS = 2
LEV_POS = 4
PARAMS_POS = 6

#TYPE = "test"
TYPE = "dev"


def parse_volume(directory, file, size):
    """
    parse for one volume (size)
    :param directory: directory to be parsed
    :param file: output filename without size string
    :param size: size string
    :return: the full filename with results and the results as string
    """
    languages = []
    results = []
    lang_folders = os.listdir(directory)
    for lang in lang_folders:
        # print(lang_folders)
        # print("\t".join(["Starting", lang, size]))
        curr_path = os.path.join(directory, lang, size)
        if (os.path.exists(curr_path)):
            dev_evals = (f for f in os.listdir(curr_path) if
                         (f.endswith("." + TYPE + "eval") or (
                         f.endswith("-eval.txt") and f.startswith(TYPE + "result"))))
            for dev_eval in dev_evals:
                if dev_eval.endswith("." + TYPE + "eval"):
                    with open(os.path.join(curr_path, dev_eval)) as f:
                        data = f.readlines()
                    line = data[0]
                    info = line.split("\t")
                    acc = info[ACC_POS]
                    lev = info[LEV_POS]
                    filename = os.path.basename(dev_eval)
                    params = re.split("-|_|\.", filename)
                    # print(params)
                    resultline = "\t".join(
                        [lang, "accuracy", str(acc), "levenshtein", str(lev), "hidden-size", params[2],
                         "embedding-size",
                         params[5], "layer-size", params[8], "patches", params[10], "enhancer", params[12], "dropout",
                         params[14], "seed", params[16]])
                    results.append(resultline)
                else:
                    with open(os.path.join(curr_path, dev_eval)) as f:
                        data = f.readlines()
                    if len(data) > 0:
                        line = data[0]
                        info = line.split("\t")
                        acc = info[ACC_POS]
                        lev = info[LEV_POS]
                        filename = os.path.basename(dev_eval)
                        resultline = "\t".join(
                            [lang, "accuracy", str(acc), "levenshtein", str(lev), "hidden-size", "-1", "embedding-size",
                             "-1", "layer-size", "-1", "patches", "False", "enhancer", "-1", "dropout",
                             "-1", "seed", "-1"])
                        results.append(resultline)

                results.append(resultline)
                # print(resultline)

    txt = create_filename(file, size)
    with open(txt, "w+") as result_file:
        result_file.write("\n".join(sorted(results)))
    return txt, results


def parse_volumes(directory, file, reparse, low, medium, high):
    """
    Parse the output for all volumes and generate one file with language, accuracy and parameters used for each run
    :param folder: folder to be parsed
    :return:
    """

    for vol in zip(["low", "medium", "high"], [low, medium, high]):
        volume = vol[0]
        if vol[1] and (reparse or not os.path.isfile(create_filename(file, volume))):
            print("Parsing: " + volume)
            parse_return = parse_volume(directory=directory, file=file, size=volume)
            print("File:\t" + parse_return[0])


def get_baseline(volume, getlev=False):
    baseline = dict()
    with open(BASELINE) as f:
        for line in f.readlines():
            if (volume + "-out") in line:
                line = line.rstrip()
                line = line.split("\t")
                lang = line[6]
                acc = line[2]
                lev = line[4]
                baseline[lang] = float(lev) if getlev else float(acc)
    return baseline


def create_matrices(directory, file, reparse, low, medium, high):
    """
    create matrices from the data. Returns one matrix with all data, one with the best accuracies for each language with the parameters, and one with the best (lowest) Levenshtein scores for all languages with the parameters.
    :param directory:
    :param file:
    :param reparse:
    :param low:
    :param medium:
    :param high:
    :return: three dictionaries with the volume as key and all data matrices, best accuracy matrices and best Levenshtein matrices as values for each volume
    """

    all_data = dict()
    acc_matrices = dict()
    lev_matrices = dict()
    for vol in zip(VOLUME_NAMES, [low, medium, high]):
        curr_vol = vol[0]
        curr_vol_bool = vol[1]
        if curr_vol_bool:
            bestaccs = dict()
            bestlevs = dict()
            data = []
            baseline = get_baseline(curr_vol)
            with open(create_filename(file, curr_vol), 'r') as f:
                for line in f.readlines():
                    line = line.rstrip()
                    line = line.split("\t")
                    maps = [str, str, float, str, float, str, int, str, int, str, int, str, bool, str, int, str, int,
                            str, int]
                    line_mapped = []
                    for i in range(len(line)):
                        if maps[i] == float:
                            line_mapped.append(float(line[i]))
                        elif maps[i] == int:
                            line_mapped.append(int(line[i]))
                        elif maps[i] == bool:
                            if line[i] == "False":
                                line_mapped.append(False)
                            elif line[i] == "True":
                                line_mapped.append(True)
                        else:
                            line_mapped.append(line[i])
                    line_mapped.append("baseline")
                    line_mapped.append(baseline[line[0]])
                    data.append(line_mapped)

                for line in data:
                    lang = line[0]
                    acc = line[2]
                    lev = line[4]
                    if not lang in bestaccs:
                        bestaccs[lang] = line
                    elif acc > bestaccs[lang][2]:
                        bestaccs[lang] = line
                    if not lang in bestlevs:
                        bestlevs[lang] = line
                    elif lev < bestaccs[lang][4]:
                        bestaccs[lang] = line

            acc_matrices[curr_vol] = sorted(bestaccs.values())
            lev_matrices[curr_vol] = sorted(bestlevs.values())
            all_data[curr_vol] = sorted(data)
    return all_data, acc_matrices, lev_matrices


def get_best_seed_matrices(data):
    """
    extracts a matrix with all lines for each language with only the best seed that resulted in the best accuracy.
    :param data: full matrix of data
    :return: matrix with all lines for each language with only the best seed that resulted in the best accuracy.
    """
    best_seed_matrices = dict()
    for vol in data.keys():
        seed_matrix = []
        best_seeds = dict()
        fullmatrix = data[vol]
        for line in fullmatrix:
            lang = line[0]
            acc = line[2]
            seed = line[18]
            if not lang in best_seeds or acc > best_seeds[lang]:
                best_seeds[lang] = seed

        for line in fullmatrix:
            lang = line[0]
            seed = line[18]
            if seed == best_seeds[lang]:
                seed_matrix.append(line)

        best_seed_matrices[vol] = seed_matrix
    return best_seed_matrices


def create_filename(file, vol):
    """
    create the full filename with the volume string
    :param file: filename without volume
    :param vol: volume string
    :return: full filename
    """
    return file + "_" + vol


def get_effect_matrix(matrices, paramslist, baseparams):
    """
    Creates matrices of the accuracy of the params listed
    :param matrices: matrices to create the new matrices from
    :param paramslist: params the effects of which should be calculated
    :param baseparams: base of the params, effect of these is 0, improvements are calculated on these
    :return: dict of matrices with volume as key
    """
    vols = matrices.keys()
    accs_all_vols = dict()
    effects_all_vols = dict()
    possible_combos_all_vol = dict()
    for vol in vols:
        matrix = matrices[vol]
        possiblevalues_dict = dict()  # dict of sets for each param of possible values
        resultdict = dict()  # final dict, with language as keys and another dict as value where each combo is a key and accuracy the value
        for param in paramslist:
            possiblevalueset = set()
            possiblevalues_dict[param] = possiblevalueset
        # go through matrix once to find all possible values for a parameter
        for row in matrix:
            # print(row)
            for param in paramslist:
                ind = row.index(param) + 1
                val = row[ind]
                possiblevalues_dict[param].add(val)
        parametercombinations = list(product(*possiblevalues_dict.values()))
        for row in matrix:
            lang = row[0]
            acc = row[2]
            if not lang in resultdict:
                curlangdict = dict()
                resultdict[lang] = curlangdict
            curr_combo = ()
            for param in paramslist:
                ind = row.index(param) + 1
                curr_combo = curr_combo + (row[ind],)
            resultdict[lang][curr_combo] = acc
        possible_combos_all_vol[vol] = parametercombinations

        effects = dict()
        for lang in resultdict.keys():
            for combo in parametercombinations:
                effectcombo = combo
                if baseparams in resultdict[lang]:
                    acc_base = resultdict[lang][baseparams]
                else:
                    acc_base = 0
                if combo in resultdict[lang]:
                    acc_combo = resultdict[lang][combo]
                else:
                    acc_base = 0
                if not lang in effects:
                    effects[lang] = dict()
                effects[lang][effectcombo] = acc_combo - acc_base

        accs_all_vols[vol] = resultdict
        effects_all_vols[vol] = effects
    return accs_all_vols, effects_all_vols, possible_combos_all_vol


def plot_effects(effect_matrix, poss_combos, num_bins, baseparams, title, filename, compare_tuples="",
                 separatezeroes=False):
    """
    plot the effects of parameters in bins
    :param effect_matrix: matrix of effects as dict
    :param poss_combos: possible combinations of parameters to be evaluated
    :param num_bins: number of bins for the effects to be separated into
    :return:
    """

    if num_bins % 2 == 0:
        num_bins += 1
    n_bins = num_bins
    combo_bins = dict()
    for combo in poss_combos:
        if not combo == baseparams:
            if separatezeroes:
                n_bins = num_bins - 1
            effects = []
            for lang in effect_matrix.keys():
                effects.append(effect_matrix[lang][combo])
            abs_effect = max(max(effects), abs(min(effects)))
            max_effect = abs_effect
            min_effect = -1.0 * abs_effect

            effectrange = max_effect - min_effect
            xaxis = []
            bin_size = effectrange / (n_bins)  # reserve one for 0
            if separatezeroes:
                xaxis.append("0")

            for i in range(n_bins + 1):
                xaxis.append(round(min_effect + i * bin_size, 1))
                # xaxis.append(
                #    str(round((min_effect + i * bin_size), 3)) + " ≤ acc < " + str(
                #        round((min_effect + (i + 1) * bin_size), 3)))

            bins = dict()
            for i in range(0, n_bins):
                bins[i] = 0
            if separatezeroes:
                bins[-1] = 0
            for lang in effect_matrix.keys():
                effect = effect_matrix[lang][combo]
                if effect == 0.0 and separatezeroes:
                    bins[-1] += 1  # bin -1 reserved for zeroes
                else:
                    bin = int((effect + abs(min_effect)) / bin_size)  # calculate the bin
                    if effect == max_effect:
                        bins[bin - 1] += 1
                    else:
                        bins[bin] += 1
            combo_bins[combo] = bins

    # plot with comparison
    for combo in poss_combos:
        combotitle = str(combo)
        ttitle = title + " " + combotitle
        if not combo == baseparams:
            bins = combo_bins[combo]
            if not compare_tuples == "":
                for compare_combo in compare_tuples:
                    compare = None
                    cc = compare_combo[0]
                    if combo == cc:
                        combo_comparewith = compare_combo[1]
                        compare = combo_bins[combo_comparewith].values()
                        comparetitle = str(compare_combo[1])
                        ttitle = ttitle + " and " + comparetitle
                        myplot(xaxis, bins.values(), ttitle,
                               filename + str(combo) + "_compared_" + str(combo_comparewith), compare_matrix=compare,
                               maintitle=combotitle, comparetitle=comparetitle, xlabel="effect on accuracy",
                               ylabel="number of languages")
            myplot(xaxis, bins.values(), ttitle, filename + str(combo), compare_matrix=None,
                   xlabel="effect on accuracy",
                   ylabel="number of languages")


def compare_folders(folder1, output1, title1, folder2, output2, title2, num_bins=0, folder1_title="folder 1",
                    folder2_title="folder 2", plotpath=""):
    sortbyvalue = True
    filename_acc = "plot_accuracy"
    filename_lev = "plot_levenshtein"

    parse_volumes(folder1, output1, reparse=True, low=True, medium=True, high=True)
    parse_volumes(folder2, output2, reparse=True, low=True, medium=True, high=True)
    data1, acc_matrices1, lev_matrices1 = create_matrices(directory=folder1, file=output1, reparse=True, low=True,
                                                          medium=True, high=True)
    data2, acc_matrices2, lev_matrices2 = create_matrices(directory=folder2, file=output2, reparse=True, low=True,
                                                          medium=True, high=True)
    acc_voldict = dict()
    lev_voldict = dict()

    for mat in zip([acc_matrices1],
                   ["accuracy for beam-size comparison"],
                   [filename_acc], [acc_matrices2]):
        matrices1 = mat[0]
        matrices2 = mat[3]
        title = mat[1]
        filename = mat[2]
        for vol in matrices1:
            matrix1 = matrices1[vol]
            matrix2 = matrices2[vol]
            xaxis = [row[0] for row in matrix1]
            acc_valuebars1 = [row[2] for row in matrix1]
            acc_valuebars2 = [row[2] for row in matrix2]
            if sortbyvalue:
                xaxis = [x for _, x in sorted(zip(acc_valuebars1, xaxis))]
                acc_valuebars2 = [x for _, x in sorted(zip(acc_valuebars1, acc_valuebars2))]
                acc_valuebars1 = sorted(acc_valuebars1)

            myplot(xaxis=xaxis, bars=acc_valuebars1, title=title,
                   filename=os.path.join(plotpath, filename + "_beamcompare_acc-" + vol),
                   maintitle=title1, comparetitle=title2,
                   compare_matrix=acc_valuebars2, xlabel="language", ylabel="accuracy")

            lev_valuebars1 = [row[4] for row in matrix1]
            lev_valuebars2 = [row[4] for row in matrix2]
            if sortbyvalue:
                xaxis = [x for _, x in sorted(zip(lev_valuebars1, xaxis))]
                lev_valuebars2 = [x for _, x in sorted(zip(lev_valuebars1, lev_valuebars2))]
                lev_valuebars1 = sorted(lev_valuebars1)
            myplot(xaxis=xaxis, bars=lev_valuebars1, title="Levenshtein-Distance for beam-size comparison",
                   filename=os.path.join(plotpath, "plot_beamcompare_lev-" + vol), maintitle=title1,
                   comparetitle=title2,
                   compare_matrix=lev_valuebars2, xlabel="language", ylabel="levenshtein-distance")

            if num_bins > 0:
                if num_bins % 2 == 0:
                    num_bins += 1
                acc_effect_of_folder_2 = [b_i - a_i for a_i, b_i in zip(acc_valuebars1, acc_valuebars2)]
                lev_effect_of_folder_2 = [b_i - a_i for a_i, b_i in zip(lev_valuebars1, lev_valuebars2)]

                len_diff = len(acc_effect_of_folder_2)

                num_plus_acc = sum(x > 0 for x in acc_effect_of_folder_2)
                num_min_acc = sum(x < 0 for x in acc_effect_of_folder_2)
                p_signs_plus = binom_test(num_plus_acc, num_plus_acc + num_min_acc, 1.0 / 2, alternative='two-sided')
                print("\t".join(
                    ["Parameter:", "Accuracy " + vol, "Num +:", str(num_plus_acc), "Num -:", str(num_min_acc),
                     "Num Signs:",
                     str(num_plus_acc + num_min_acc), "binom. p (+):", str(p_signs_plus)]))

                num_plus_lev = sum(x > 0 for x in lev_effect_of_folder_2)
                num_min_lev = sum(x < 0 for x in lev_effect_of_folder_2)
                p_signs_plus = binom_test(num_plus_lev, num_plus_lev + num_min_lev, 1.0 / 2, alternative='two-sided')
                print("\t".join(
                    ["Parameter:", "Levenshtein " + vol, "Num +:", str(num_plus_lev), "Num -:", str(num_min_lev),
                     "Num Signs:",
                     str(num_plus_lev + num_min_lev), "binom. p (+):", str(p_signs_plus)]))

                for effect_zip in zip([acc_effect_of_folder_2, lev_effect_of_folder_2], ["accuracy", "levenshtein"]):
                    effects = effect_zip[0]
                    abs_effect = max(max(effects), abs(min(effects)))
                    max_effect = abs_effect
                    min_effect = -1.0 * abs_effect
                    effectrange = max_effect - min_effect
                    bin_size = effectrange / (num_bins)
                    xaxis = []
                    decimals = 1 if effect_zip[1] == "accuracy" else 2
                    for i in range(num_bins + 1):
                        xaxis.append(round(min_effect + i * bin_size, decimals))
                    bins = dict()
                    for i in range(0, num_bins):
                        bins[i] = 0
                    for effect in effects:
                        bin = int((effect + abs(min_effect)) / bin_size)  # calculate the bin
                        if effect == max_effect:
                            bins[bin - 1] += 1
                        else:
                            bins[bin] += 1
                    filename_path = os.path.join(plotpath, filename + "_folder_comparison_" + effect_zip[1] + "_" + vol)
                    myplot(xaxis, bins.values(),
                           "effect of {name2} on {value} compared to {name1} on {size}".format(name1=folder1_title,
                                                                                               value=effect_zip[1],
                                                                                               name2=folder2_title,
                                                                                               size=vol),
                           filename_path,
                           compare_matrix=None,
                           xlabel="effect on {value}".format(value=effect_zip[1]),
                           ylabel="number of languages")


def sign_test(matrix, parameter, no_effect, possible_effects):
    infoarray = []
    signarray = []
    for lang in matrix.keys():
        no_effect_val = matrix[lang][no_effect]
        effect_val = matrix[lang][possible_effects[0]]
        for eff in possible_effects:
            poss_effect_val = matrix[lang][eff]
            if poss_effect_val > effect_val:
                effect_val = poss_effect_val
        effect = effect_val - no_effect_val
        infoarray.append([lang, no_effect_val, effect_val, effect])
        sign = -1 if effect < 0 else 1 if effect > 0 else 0
        if sign != 0: signarray.append(sign)
    sum_signs = sum(signarray)
    num_plus = signarray.count(1)
    num_minus = signarray.count(-1)
    len_signs = len(signarray)  #
    p_signs_plus = binom_test(num_plus, len_signs, 1.0 / 2, alternative='two-sided')
    print("\t".join(
        ["Parameter:", parameter, "Num +:", str(num_plus), "Num -:", str(num_minus), "Num Signs:", str(len_signs),
         "binom. p (+):", str(p_signs_plus)]))
    return signarray, sum_signs, len_signs, p_signs_plus


def main(directory, file, reparse, low, medium, high, plotpath, sortbyvalue, showbaseline, separatezeroes,
         test_enhancer=False, test_patches=False, ploteffects=False, sign_test_bool=False):
    if not (low or medium or high) and reparse:
        print("Cannot reparse with no volume given!")
    parse_volumes(directory, file, reparse, low, medium, high)  # create file

    if plotpath != "":
        print("Plotting...")
        if not os.path.isdir(plotpath):
            os.makedirs(plotpath)
        # create matrices (dicts with volumes as key and matrices as value)
        data, acc_matrices, lev_matrices = create_matrices(directory=directory, file=file,
                                                           reparse=reparse, low=low, medium=medium,
                                                           high=high)

        # get matrices with best seed for each language (dict)
        best_seed_matrices = get_best_seed_matrices(data)

        # get matrices of language with accuracy per enhancer/patch combination
        if test_patches and test_enhancer:
            patch_enhancer_matrix, patch_enhancer_effects, poss_combos = get_effect_matrix(best_seed_matrices,["patches", "enhancer"],
                                                                                       (False, 0))
        elif test_patches and not test_enhancer:
            patch_enhancer_matrix, patch_enhancer_effects, poss_combos = get_effect_matrix(best_seed_matrices,["patches"],
                                                                                       (False,))
        elif not test_patches and test_enhancer:
            patch_enhancer_matrix, patch_enhancer_effects, poss_combos = get_effect_matrix(best_seed_matrices,
                                                                                       ["enhancer"],
                                                                                       (0,))

        # plot effects
        if ploteffects:
            for vol in zip((low, medium, high), VOLUME_NAMES):
                if vol[0]:
                    if test_patches and test_enhancer:
                        plot_effects(patch_enhancer_effects[vol[1]], poss_combos[vol[1]], 7, (False, 0),
                                     "Effect Patches/Enhancer on Accuracy compared to no Patches, no Enhancer;",
                                     os.path.join(PROJECT, "output", "plot", "effect_patches_enhancer_plot-" + vol[1]),
                                     compare_tuples=[((False, 5), (False, 1))],
                                     separatezeroes=separatezeroes)
                    elif test_patches and not test_enhancer:
                        patch_enhancer_effects_mat = patch_enhancer_effects[vol[1]]
                        plot_effects(patch_enhancer_effects_mat, poss_combos[vol[1]], 7, (False,),
                                     "Effect Patches on Accuracy compared to no Patches;",
                                     os.path.join(PROJECT, "output", "plot", "effect_patches_plot-" + vol[1]),
                                     separatezeroes=separatezeroes)
                    elif not test_patches and test_enhancer:
                        plot_effects(patch_enhancer_effects[vol[1]], poss_combos[vol[1]], 7, (0),
                                     "Effect Enhancer on Accuracy compared to no Enhancer;",
                                     os.path.join(PROJECT, "output", "plot", "effect_enhancer_plot-" + vol[1]),
                                     separatezeroes=separatezeroes)

        # sign test
        if sign_test_bool:
            if test_patches and test_enhancer:
                patch_matrix = get_effect_matrix(best_seed_matrices, ["patches"], (False,))[1]
                enhancer_matrix = get_effect_matrix(best_seed_matrices, ["enhancer"], (0,))[1]

                for vol in zip((low, medium, high), VOLUME_NAMES):
                    if vol[0]:
                        sign_test(enhancer_matrix[vol[1]], "enhancer", (0,), [(1,), (5,)])
                        sign_test(patch_matrix[vol[1]], "patches", (False,), [(True,)])
            elif test_patches and not test_enhancer:
                patch_matrix = get_effect_matrix(best_seed_matrices, ["patches"], (False,))[1]

                for vol in zip((low, medium, high), VOLUME_NAMES):
                    if vol[0]:
                        sign_test(patch_matrix[vol[1]], "patches", (False,), [(True,)])
            elif not test_patches and test_enhancer:
                enhancer_matrix = get_effect_matrix(best_seed_matrices, ["enhancer"], (0,))[1]

                for vol in zip((low, medium, high), VOLUME_NAMES):
                    if vol[0]:
                        sign_test(enhancer_matrix[vol[1]], "enhancer", (0,), [(1,), (5,)])

        # plot_matrices
        for mat in zip([acc_matrices], ["Best Accuracies"], ["plot_accuracy"]):
            matrices = mat[0]
            title = mat[1]
            filename = mat[2]
            for vol in matrices:
                matrix = matrices[vol]
                xaxis = [row[0] for row in matrix]
                valuebars = [row[2] for row in matrix]
                baselinebars = [row[20] for row in matrix]
                if sortbyvalue:
                    xaxis = [x for _, x in sorted(zip(valuebars, xaxis))]
                    baselinebars = [x for _, x in sorted(zip(valuebars, baselinebars))]
                    valuebars = sorted(valuebars)
                if not showbaseline:
                    baselinebars = None
                myplot(xaxis=xaxis, bars=valuebars, title=title, filename=os.path.join(plotpath, filename + "-" + vol),
                       maintitle="UHH", comparetitle="baseline",
                       compare_matrix=baselinebars)

                # plot acc difference baseline/UHH
                diff_acc = [a_i - b_i for a_i, b_i in zip(valuebars, baselinebars)]
                if sortbyvalue:
                    xaxis = [x for _, x in sorted(zip(diff_acc, xaxis))]
                    diff_acc = sorted(diff_acc)
                myplot(xaxis=xaxis, bars=diff_acc, title="Accuracy difference UHH/Baseline on " + vol,
                       filename=os.path.join(plotpath, "plof-diff_acc_baseline-" + vol), grid=True)

                # plot lev
                baseline = get_baseline(vol, getlev=True)

                lev_bars = [row[4] for row in matrix]
                xaxis = [row[0] for row in matrix]
                lev_dict = dict()
                for l in xaxis:
                    lev_base = baseline[l]
                    lev_dict[l] = lev_base
                lev_base_bars = lev_dict.values()
                if sortbyvalue:
                    xaxis = [x for _, x in sorted(zip(lev_bars, xaxis), reverse=True)]
                    lev_base_bars = [x for _, x in sorted(zip(lev_bars, lev_base_bars), reverse=True)]
                    lev_bars = sorted(lev_bars, reverse=True)
                myplot(xaxis=xaxis, bars=lev_bars, title="Levenshtein UHH and baseline on " + vol,
                       filename=os.path.join(plotpath, "plot-lev-" + vol), maintitle="UHH", comparetitle="baseline",
                       compare_matrix=lev_base_bars)

                # plot lev difference

                diff_dict = dict()
                for row in matrix:
                    l = row[0]
                    diff_dict[l] = row[4] - baseline[l]
                xaxis = diff_dict.keys()
                diff_lev = diff_dict.values()
                if sortbyvalue:
                    xaxis = [x for _, x in sorted(zip(diff_lev, xaxis), reverse=True)]
                    diff_lev = sorted(diff_lev, reverse=True)
                myplot(xaxis=xaxis, bars=diff_lev, title="Levenshtein difference UHH/Baseline on " + vol,
                       filename=os.path.join(plotpath, "plot-diff_lev_baseline-" + vol), grid=True)


def myplot(xaxis, bars, title, filename, compare_matrix=None, maintitle="", comparetitle="", xlabel="", ylabel="",
           grid=False):
    print("Plotting:\t" + filename)
    plt.grid(grid)
    move_xaxis = 0.0 if compare_matrix is None else -0.4
    indices = [ind + move_xaxis for ind in np.arange(len(bars))]
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)

    deg = '90' if len(indices) > 20 else '0'
    plt.xticks([ind + move_xaxis for ind in np.arange(len(xaxis))], xaxis, rotation=deg)
    align = 'center' if len(indices) > 20 else 'edge'
    width = 0.25 if len(indices) > 20 else 0.4
    # plt.title(title)
    if not compare_matrix is None:

        plt.bar(indices, bars, width=width, label=maintitle, fill=False, hatch="/", edgecolor='black', align=align)
        indices_compare = [ind + 0.5 for ind in indices]
        plt.bar(indices_compare, compare_matrix, width=width, label=comparetitle, color='black', align=align)
        plt.legend()
        # plt.xlim(-0.75,len(indices)-0.25)
    else:
        plt.bar(indices, bars, width=1, color='#808080', align='edge')
        plt.xlim(-0.25, len(indices) + 0.25)

    fig = plt.gcf()
    figsize = (14, 8) if len(indices) > 20 else (3, 3)
    fig.set_size_inches(figsize)
    if not filename == "":
        pass
        plt.savefig(filename + ".pdf")
    # plt.show()
    fig.clear()


# if __name__ == '__main__':
if False:
    parser = argparse.ArgumentParser(description='Extracts results from log file and plots them')
    parser.add_argument('-d', '--directory', type=str, dest='directory', help='Directory with the output to be parsed',
                        required=True)
    parser.add_argument('-f', '--file', type=str, dest='output_file',
                        help='file (with path) with the parsed results. Will be created if it does not exist. the volume will be appended to the name',
                        required=True)
    parser.add_argument('-r', '--reparse', action='store_true', help='Reparse the output file if it exists already.',
                        default=False)
    parser.add_argument('-l', '--low', action='store_true', help='Parse low volume output')
    parser.add_argument('-m', '--medium', action='store_true', help='Parse medium volume output')
    parser.add_argument('-i', '--high', action='store_true', help='Parse high volume output')
    parser.add_argument('-p', '--plot', type=str, dest='plot', help='Plot path', default='')
    parser.add_argument('-s', '--sortbyvalue', action='store_true', help='sort the bars in the plot by value, not lang')
    parser.add_argument('-b', '--baseline', action='store_true', help='show baseline in the accuracy/levenshtein plots')
    parser.add_argument('--separatezeroes', action='store_true', help='Separate the Zeroes in the effect plots')
    parser.add_argument('--patches', action='store_true', help='Test the effect of Patches')
    parser.add_argument('--enhancer', action='store_true', help='Test the effect of the Enhancer')

    args = parser.parse_args()
    print("Args: " + str(args))

    main(directory=args.directory, file=args.output_file,
         reparse=args.reparse, low=args.low, medium=args.medium,
         high=args.high, plotpath=args.plot, showbaseline=args.baseline)


def run_main():
    print("main1")
    main(directory="/home/marcel/git/prost18-sigmorphon/output/compare_medium_patches", file="output_parser_medium",
         reparse=True, low=False, medium=True, high=False, plotpath="output/plot",
         sortbyvalue=True, showbaseline=True, separatezeroes=False, ploteffects=True, test_patches=True,
         test_enhancer=False, sign_test_bool=True)


def run_compare():
    compare_folders("/home/marcel/git/prost18-sigmorphon/output/beam_compare/beam1",
                    "output_compare_beam1", "beam-size 1",
                    "/home/marcel/git/prost18-sigmorphon/output/beam_compare/beam16",
                    "output_compare_beam16", "beam-size 16", 6, folder1_title="beam 1", folder2_title="beam 16",
                    plotpath="/home/marcel/git/prost18-sigmorphon/output/plot")


run_main()
#run_compare()
