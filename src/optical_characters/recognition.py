from typing import Dict, Callable

import numpy as np
from optical_characters.util import mse_matrices


def matrix_to_char(matrix: np.ndarray, comparison: Dict[str, np.ndarray], loss: Callable[[np.ndarray, np.ndarray], float] = None) -> str:
    """
    Guess which character is represented by :param matrix, based on a loss function calculation

    The user has to provide a dictionary of options to check against,
    where the key is the string representation and the value is a NumPy matrix

    :param matrix: The matrix to identify
    :param comparison: A dictionary of the form {string: rendered matrix} to compare entries against
    :param loss: The loss function to use for computing arg_min. Defaults to MSE

    :return: The string that is most likely based on the loss function provided
    """
    if loss is None:
        # nasty hack because callable parameters do not support default arguments
        loss = mse_matrices

    mean_errors = {k: loss(matrix, v) for k, v in comparison.items()}
    return min(mean_errors, key=mean_errors.get)
