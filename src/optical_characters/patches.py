import json
import os
from functools import reduce
from itertools import combinations, permutations
from typing import Callable, Iterable, TypeVar, Dict, Set

from unidecode import unidecode
from unicodedata import normalize

from operator import and_, eq

from optical_characters.rasterization import char_to_matrix
from optical_characters.util import patch_matrix, trim_array_equal, align_matrix, exact_array_equal

T = TypeVar('T')


def contains(collection: Iterable[T], data: T, compare: Callable[[T, T], bool]) -> bool:
    """
    Check whether an element is contained in an iterable list of things,
    based on a user-defined predicate (basically "in" operator with custom comparison instead of ==)

    :param collection:  The collection of items
    :param data:        The item to look for
    :param compare:     A function that takes two args of the same type as "data" as input and returns a bool

    :return: Whether the element is included in the collection, based on your comparison fn input
    """
    return any(compare(data, x) for x in collection)


def unique(collection: Iterable[T], compare: Callable[[T, T], bool]) -> Iterable[T]:
    """
    Turn a list into a collection of only unique elements of that list,
    where "unique" is defined by a custom predicate (basically "set" with custom comparison instead of ==)

    Later items (based on iteration order) are shadowed by earlier identical items.

    :param collection:  The collection to process
    :param compare:     A function that takes two args of the same type as the list elements as input and returns a bool

    :return:
    """
    uniq_collection = []

    for elem in collection:
        if not contains(uniq_collection, elem, compare):
            uniq_collection.append(elem)

    return uniq_collection


def find_all_keys(my_dict: Dict[str, T], item: T, compare: Callable[[T, T], bool]) -> Iterable[str]:
    """
    Iterate over a dictionary and return a list of keys whose associated element(s) match the user-given predicate

    :param my_dict: Dictionary to process
    :param item:    The item to compare list entries against
    :param compare: A function that takes two args of the same type as "item" as input and returns a bool

    :return:
    """
    return [k for k, v in my_dict.items() if compare(item, v)]


def generate_patches(chars: Iterable[str]):
    """
    Generate a patch table for a given input alphabet.

    The table is arranged as list where every list index is the number that identifies the patch.
    Each list entry (i.e. patch in the patch table) is a list itself containing tuples of (c_in, c_out) configurations
    that are grouped into symmetric pairs.

    :param chars: Characters for which to consider patches. Ideally the distinct set of input letters for training data

    :return: A list where every index represents a patch number and the entry is the patch group (description above)
    """
    indexed_patches = _generate_font_patches(chars)

    # add symmetry so that (a, b) also yields (b, a)
    symmetric = [[t for perms in map(permutations, tuples) for t in perms] for tuples in indexed_patches]

    lang_chars = set([c for c in chars if len(c) == 1])
    lang_combos = [combo for combo in combinations(lang_chars, 2)]

    return [lang_patches for lang_patches in symmetric if any(map(lambda p: p in lang_combos, lang_patches))]


def _generate_font_patches(chars: Iterable[str]):
    """
    Generate patches between similar symbols based on pygame renderings.
    INTERNAL STUFF. You probably want "generate_patches" instead.

    :param chars: Characters to consider/render

    :return: Asymmetrical patch entries
    """
    dir_name = os.path.realpath(os.path.dirname(__file__))

    # good results for futura, space, ubuntu
    font_name = 'futura'
    font = os.path.join(dir_name, 'font', font_name + '.ttf')
    size = 36

    # the typical "character not displayable" / "not supported by font" rectangle
    heuristics_unknown_char = char_to_matrix('\uFFFD', font, size)
    heuristics_threshold = (size / 4) ** 2

    dev_chars = set(map(chr, range(1, max(map(ord, 'äöüïëßıů' + str(chars))) + 1)))

    replacements = {s: s for s in dev_chars}
    replacements['i'] = 'ı'
    replacements['İ'] = 'I'  # FIXME do we need this replacement?

    # find a large enough range of chars and map them to a {char: matrix} dict
    char_matrices = {s: char_to_matrix(replacements.get(s, s), font, size) for s in dev_chars}

    # heuristic to prevent combinatorial explosion
    dev_chars = {c for c in dev_chars if not exact_array_equal(char_matrices[c], heuristics_unknown_char)}

    corr_dict_path = os.path.realpath(os.path.join(dir_name, 'corrections', font_name + '.json'))

    with open(corr_dict_path, encoding='utf-8') as fp:
        corr_dict = json.load(fp)

    corrected = {c: align_matrix(char_matrices[c], *off) for c, off in corr_dict.items()}
    char_matrices.update(corrected)

    # find combination tuples (of length 2) that are based on the same ASCII char
    combos = [combo for combo in combinations(dev_chars, 2) if len(set(map(unidecode, combo))) == 1]

    # unfold the combination 2-tuples to actual patch matrices
    patches_dict = {tuple(sorted(combo)): patch_matrix(*map(char_matrices.get, combo)) for combo in combos}

    # filter out empty patches as well as too large patches
    relevant_patches = [p for p in patches_dict.values() if p.any() and p.sum() < heuristics_threshold]

    # collapse identical patches irrespective of position within frame
    unique_patches = unique(relevant_patches, trim_array_equal)

    # check what combinations those unique patches belong to
    return [find_all_keys(patches_dict, p, trim_array_equal) for p in unique_patches]


def _generate_nfd_patches(chars: Iterable[str]):
    """
    EXPERIMENTAL. Read at your own risk.

    :param chars: Characters to decompose

    :return: Asymmetrical patch group entries
    """
    dev_chars = set(map(chr, range(1, max(map(ord, 'äöüïëßıů' + str(chars))) + 1)))

    # find a large enough range of chars and map them to a {char: matrix} dict
    char_decompositions = {s: set(normalize('NFD', s)) for s in dev_chars}

    # find combination tuples (of length 2) that are based on the same ASCII char
    combos = [sorted(combo) for combo in combinations(dev_chars, 2) if
              reduce(and_, map(char_decompositions.get, combo)) == set(map(unidecode, combo))]

    # unfold the combination 2-tuples to actual patch matrices
    patches_dict = {tuple(combo): patch_diff(*map(char_decompositions.get, combo)) for combo in combos}

    # collapse identical patches irrespective of position within frame
    unique_patches = set(patches_dict.values())

    # check what combinations those unique patches belong to
    return [find_all_keys(patches_dict, p, eq) for p in unique_patches]


def patch_diff(decomp_a: Set[str], decomp_b: Set[str]):
    """
    EXPERIMENTAL. Go away.

    :param decomp_a:
    :param decomp_b:
    :return:
    """
    intersect = decomp_a & decomp_b

    return first_or_none(decomp_a - intersect), first_or_none(decomp_b - intersect)


def first_or_none(itr):
    """
    EXPERIMENTAL. Do not touch or your hamster will explode.

    :param itr:
    :return:
    """
    for v in itr:
        return v

    return None
