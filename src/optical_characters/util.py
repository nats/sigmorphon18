from typing import Iterable
import numpy as np


def concat_matrices(*matrix) -> np.ndarray:
    """
    Concatenate pixel matrices horizontally

    :param matrix: As many matrices as you would like to concatenate. Shape must match along axis 1
    :return: One big concatenated matrix
    """
    return np.concatenate(matrix, axis=1)


def mse_matrices(mat_a: np.ndarray, mat_b: np.ndarray) -> float:
    """
    Determine the mean squared error between two numerical matrices

    :param mat_a: First matrix
    :param mat_b: Second matrix

    :return: MSE between mat_a and mat_b
    """
    square_diff = (mat_a - mat_b) ** 2
    return square_diff.mean(axis=None)


def diff_sum_matrices(mat_a: np.ndarray, mat_b: np.ndarray) -> int:
    """
    Determine the number of cells that are not equal between two matrices

    :param mat_a: First matrix
    :param mat_b: Second matrix

    :return: Number of cells that are not equal
    """
    diff = np.logical_and(mat_a, mat_b)
    return np.logical_not(diff).sum(axis=None)


def patch_matrix(mat_a: np.ndarray, mat_b: np.ndarray) -> np.ndarray:
    """
    Calculate the patch that transforms one pixel matrix into another pixel matrix (and vice versa)

    :param mat_a: One pixel matrix (2D array with only 0 and 1)
    :param mat_b: Another pixel matrix

    :return: The pixel patch between a and b
    """
    return np.where(np.logical_xor(mat_a, mat_b), 1, 0)


def trim_matrix(mat: np.ndarray) -> np.ndarray:
    """
    Trim off empty rows and columns (i.e. that only contain the value 0)

    :param mat: Matrix to trim

    :return: Trimmed matrix
    """
    mat = mat[(mat != 0).any(axis=1)]  # trim off "all-zero" rows
    mat = mat[:, (mat != 0).any(axis=0)]  # trim off "all-zero" columns

    return mat


def pad_matrix(mat: np.ndarray, shape, center: bool = True) -> np.ndarray:
    """
    Pad matrix into desired size by adding rows and/or columns that only contain 0s

    Centering centers the matrix in the final result (duh) whereas not centering
    simply appends rows on the right and bottom ends

    :param mat:     Matrix to pad
    :param shape:   The desired target shape to pad to
    :param center:  Whether to center the origin matrix in the new padded grid or not

    :return: A padded matrix
    """
    ref = np.zeros(shape)

    off = [int((ref.shape[dim] - mat.shape[dim]) / 2) for dim in range(mat.ndim)] if center else [0] * mat.ndim

    ins = [slice(off[dim], off[dim] + mat.shape[dim]) for dim in range(mat.ndim)]
    ref[ins] = mat

    return ref


def align_matrix(mat: np.ndarray, *offset) -> np.ndarray:
    """
    Re-align matrix per axis with the given offset(s) by rolling over.
    Basically shift along dimension n by offset[n] rows/columns.

    :param mat:     Matrix to align
    :param offset:  Offset(s) by which to align

    :return: Aligned matrix
    """
    for axis, off in enumerate(offset):
        mat = np.roll(mat, off, axis)

    return mat


def pad_matching_matrices(*mat: np.ndarray, center: bool = True) -> Iterable[np.ndarray]:
    """
    Pad matrices so that they are guaranteed to have the same size afterwards.
    Achieved by padding both to the "max" dimensions necessary to fit either matrix

    :param mat:     Matrices to pad mutually
    :param center:  Whether to center while padding (see "pad_matrix")

    :return: An iterable of the same lenght as "mat" where every entry is the n-th matrix from "mat" but padded
    """
    max_dim = tuple(map(max, zip(*map(lambda x: x.shape, mat))))
    return map(lambda x: pad_matrix(x, max_dim, center), mat)


def trim_array_equal(x: np.ndarray, y: np.ndarray) -> bool:
    """
    Check whether two matrices are equal after trimming off 0-only rows/columns

    :param x: First matrix
    :param y: Second matrix

    :return: Whether they are equal or not
    """
    return np.array_equal(trim_matrix(x), trim_matrix(y))


def exact_array_equal(x: np.ndarray, y: np.ndarray) -> bool:
    """
    Alias to np.array_equal
    """
    return np.array_equal(x, y)


def threshold_array_equal(x: np.ndarray, y: np.ndarray, t: float = .3) -> bool:
    """
    Check whether two matrices are equal up to a certain MSE threshold.
    Calculates MSE first and checks whether it falls below the desired threshold.

    :param x: First matrix
    :param y: Second matrix
    :param t: Threshold

    :return: Whether the input is below threshold
    """
    return mse_matrices(*pad_matching_matrices(trim_matrix(x), trim_matrix(y))) < t


def pixel_array_equal(x: np.ndarray, y: np.ndarray, t: float = .3) -> bool:
    """
    Check whether two matrices are equal up to a certain pixel threshold.

    Calculates the number of different pixels first and checks whether it is less than the desired threshold
    *in relation to the average total number of pixels"!

    :param x: First matrix
    :param y: Second matrix
    :param t: Threshold

    :return: Whether the proportion of pixel similarity is below threshold
    """
    diff = patch_matrix(*pad_matching_matrices(trim_matrix(x), trim_matrix(y)))
    avg_sum = (x.sum() + y.sum()) / 2

    return diff.sum() / avg_sum < t


def pixel_contains(container: np.ndarray, check: np.ndarray) -> bool:
    """
    Check whether the pixel matrix "check" is completely contained in "container".
    For example, the pixel matrix representing the letter "a" would be completely contained in the pixel matrix for "à"

    :param container:   The container
    :param check:       The item for which to check whether it is inside the container

    :return: Whether the item is completely contained in the surrounding container
    """
    return np.array_equal(np.logical_and(container, check), check)


def display(char_mat: np.ndarray):
    """
    Print a pixel matrix to the standard output your python interpreter is attached to.
    Set/black pixels will be represented by '#', blank/white pixels by blank space (duh)

    :param char_mat: The pixel matrix to print
    """
    result = np.where(char_mat, '#', ' ')
    print('\n'.join([''.join(row) for row in result]))
