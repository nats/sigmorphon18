import numpy as np
from pygame import ftfont, pixelcopy

from optical_characters.util import pad_matrix

ftfont.init()

font_pool = {}


def char_to_matrix(text: str, font_path: str, render_size: int,
                   anti_aliasing: bool = False) -> np.ndarray:
    """
    Convert a given string of text into a numpy matrix
    where 1 is a set (black) pixel and 0 is an unset (white) pixel

    :param text: The character to convert
    :param font_path: The path of the (TrueType) font to use
    :param render_size: The font size in which to render individual characters
    :param anti_aliasing: Whether to use anti-aliasing or not

    :return: A NumPy binary matrix representing a char grid of the input string
    """
    # TODO iterate recursively over characters of string if len > 1 ??
    # if len(text) > 1:
    #     return concat_matrices(*map(lambda x: char_to_matrix(x, font_path, render_size, anti_aliasing), text))

    font = font_pool.get(font_path, ftfont.Font(font_path, render_size))
    font_pool[font_path] = font

    render = font.render(text, anti_aliasing, (0, 0, 0), (255, 255, 255))

    pixel_map = np.zeros(render.get_size() + (3,), "uint8")
    pixelcopy.surface_to_array(pixel_map, render)

    pixel_map = np.mean(pixel_map, 2).T

    my_size = tuple([max(pixel_map.shape)] * 2)
    pixel_map = np.where(pixel_map == 255, 0, 1)

    return pad_matrix(pixel_map, my_size)
