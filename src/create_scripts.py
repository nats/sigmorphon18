#! /usr/bin/env python3

import os

from project_base import LANGUAGES, LANG_ENVIRONMENTS, PROJECT_SRC, PYTHON_ENV, output_path

SCRIPT_FILEPATH = os.path.join(PROJECT_SRC, 'search_parameters.py')

for lang in LANGUAGES:
    for size in LANG_ENVIRONMENTS:
        output_text = "#!/bin/bash\nsource ~/miniconda3/bin/activate pytorch\nkrenew -- {s} -l \"{g}\" -e \"{e}\" -m \"{m}\" -p best_lev_params_{e}.txt".format(
            s=SCRIPT_FILEPATH,
            g=lang,
            e=size,
            m='fst')  # TODO allow other models

        with open(output_path('script', file='-'.join([lang, size]) + '.sh'), mode='w+') as f:
            f.write(output_text + '\n')
