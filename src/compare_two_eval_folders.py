#! /usr/bin/env python3

from plot_results_folder_structure import compare_two_folders
from project_base import OUTPUT, ENHANCED
from os import path
import argparse

def main(folderOld, folderNew, vol="low"):
    compare_two_folders(folderNew, folderOld, volume=vol)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Compares two evaluation results)')
    parser.add_argument('-o', '--old', dest='folderOld', default=path.join(OUTPUT, "evaluation"))
    parser.add_argument('-n', '--new', dest='folderNew', default=path.join(OUTPUT, "evaluation_patches"))
    parser.add_argument('-v', '--volume', dest='volume', default="low")
    args = parser.parse_args()
    main(args.folderOld, args.folderNew, args.volume)