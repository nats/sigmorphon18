import importlib
import os
import sys

PROJECT = os.path.realpath(os.path.dirname(os.path.dirname(__file__)))

OUTPUT = os.path.join(PROJECT, 'output')
PLOTFOLDER = os.path.join(PROJECT, 'plots')
PROJECT_SRC = os.path.join(PROJECT, 'src')
ENHANCED = os.path.join(PROJECT, 'enhanced')
PLACEHOLDER_ENHANCER = ">"

sys.path.append(PROJECT_SRC)

PYTHON_ENV = 'python3'

LANGUAGES =['adyghe',
            'albanian',
            'arabic',
            'armenian',
            'asturian',
            'azeri',
            'bashkir',
            'basque',
            'belarusian',
            'bengali',
            'breton',
            'bulgarian',
            'catalan',
            'classical-syriac',
            'cornish',
            'crimean-tatar',
            'czech',
            'danish',
            'dutch',
            'english',
            'estonian',
            'faroese',
            'finnish',
            'french',
            'friulian',
            'galician',
            'georgian',
            'german',
            'greek',
            'greenlandic',
            'haida',
            'hebrew',
            'hindi',
            'hungarian',
            'icelandic',
            'ingrian',
            'irish',
            'italian',
            'kabardian',
            'kannada',
            'karelian',
            'kashubian',
            'kazakh',
            'khakas',
            'khaling',
            'kurmanji',
            'ladin',
            'latin',
            'latvian',
            'lithuanian',
            'livonian',
            'lower-sorbian',
            'macedonian',
            'maltese',
            'mapudungun',
            'middle-french',
            'middle-high-german',
            'middle-low-german',
            'murrinhpatha',
            'navajo',
            'neapolitan',
            'norman',
            'northern-sami',
            'north-frisian',
            'norwegian-bokmaal',
            'norwegian-nynorsk',
            'occitan',
            'old-armenian',
            'old-church-slavonic',
            'old-english',
            'old-french',
            'old-irish',
            'old-saxon',
            'pashto',
            'persian',
            'polish',
            'portuguese',
            'quechua',
            'romanian',
            'russian',
            'sanskrit',
            'scottish-gaelic',
            'serbo-croatian',
            'slovak',
            'slovene',
            'sorani',
            'spanish',
            'swahili',
            'swedish',
            'tatar',
            'telugu',
            'tibetan',
            'turkish',
            'turkmen',
            'ukrainian',
            'urdu',
            'uzbek',
            'venetian',
            'votic',
            'welsh',
            'west-frisian',
            'yiddish',
            'zulu']

LANG_ENVIRONMENTS = [
    'high',
    'medium',
    'low'
]

NETWORKS = {
    'fst': 'FstNetwork',
    'char_rnn': 'CharNetwork'
}

def enhanced_data(lang, tag, n):
    n_subfolder = enhanced_data_folder(n)
    filename = enhanced_data_filename(lang, tag)
    return os.path.realpath(os.path.join(n_subfolder, filename))

def enhanced_data_filename(lang, tag):
    filename = '-'.join(['enhanced', lang, 'train', tag])
    return filename

def enhanced_data_folder(n):
    n_subfolder = '_'.join(['enhanced', str(n)])
    return os.path.realpath(os.path.join(ENHANCED, n_subfolder))

def language_training(lang, tag):
    return os.path.realpath(os.path.join(PROJECT, 'data', '-'.join([lang, 'train', tag])))

def language_dev(lang):
    return os.path.realpath(os.path.join(PROJECT, 'data', '-'.join([lang, 'dev'])))

def language_test(lang):
    return os.path.realpath(os.path.join(PROJECT, 'data', '-'.join([lang, 'covered','test'])))

def training_output(lang, tag):
    return os.path.realpath(os.path.join(OUTPUT, 'train', '-'.join([lang, tag])))

def dev_output(lang):
    return os.path.realpath(os.path.join(OUTPUT, 'dev', lang))

def test_output(lang):
    return os.path.realpath(os.path.join(OUTPUT, 'test', lang))

def best_params_path(filename):
    return os.path.realpath(os.path.join(PROJECT, filename))

def output_path(*module, file):
    output_dir = os.path.realpath(os.path.join(OUTPUT, *module))

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    return os.path.realpath(os.path.join(output_dir, file))


def create_network(net, **kwargs):
    net = str(net).lower()

    module = importlib.import_module('.'.join(['networks', net]))
    net_class = getattr(module, NETWORKS[net])

    return net_class(**kwargs)
