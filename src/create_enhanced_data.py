#! /usr/bin/env python3
from enhancer.enhance_data_to_file import write_random_data_for_whole_folder, write_random_data
from project_base import ENHANCED, PROJECT

import os
def create_data():
    for n in [1,3,5,10,20,50]:
        foldername = "_".join(["enhanced", str(n)])
        #write_random_data_for_whole_folder(folder="../data/", num=n, file_filter="-train-low", outputfolder=os.path.join(PROJECT, "enhanced", foldername))
        write_random_data_for_whole_folder(folder="/home/marcel/git/conll2018/task1/surprise/", num=n, file_filter="-train-low", outputfolder=os.path.join(PROJECT, "enhanced_surprise", foldername))
        print("\t".join(["Written:", str(n)]))
    #write_random_data_for_whole_folder(folder="/home/marcel/git/conll2018/task1/all/", num=20, file_filter="-train-medium")
#create_data()

#write_random_data(name="asturian-train-low-enhanced", file=os.path.join("/home/marcel/git/prost18-sigmorphon/data", "asturian-train-low"), num=1)
#write_random_data(name="basque-train-low-enhanced", file=os.path.join("/home/marcel/git/prost18-sigmorphon/data", "basque-train-low"), num=20)
#write_random_data(name="middle-high-german-train-low-enhanced", file=os.path.join("/home/marcel/git/prost18-sigmorphon/data", "middle-high-german-train-low"), num=20)
#write_random_data(name="middle-high-german-train-low-enhanced", file=os.path.join("/home/marcel/git/prost18-sigmorphon/data", "navajo-train-low"), num=20)
write_random_data(name="english-train-low-enhanced", file=os.path.join("../data", "english-train-low"), num=1)