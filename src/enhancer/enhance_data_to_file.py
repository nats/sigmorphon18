import os
from project_base import ENHANCED, PROJECT
from enhancer.enhance_data import get_random_words_by_file
from enhancer.util import get_lang_model

sub_folder = ENHANCED


def write_random_data(name, file, num, output_folder=sub_folder, min_supp=2, max_supp=0):
    """
    Generates random words based on words from the file. It writes these words to a file.

    :param name: filename to be written to
    :param file: file to generate data from
    :param num: number of words to generate per match
    :param output_folder: the folder to write to
    :param alphabet: alphabet to be used to generate the words
    """
    min_support=min_supp
    max_support=max_supp
    lang_model = get_lang_model(file)
    data = get_random_words_by_file(file, num, lang_model, min_supp=min_support, max_supp=max_support)
    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)
    filepathname = os.path.join(output_folder, name)
    with open(filepathname, "w+", encoding="utf-8") as f:
        for row in data:
            f.write(row[0] + "\t" + row[1] + "\t" + row[2] + "\n")
    print("Created: "+ filepathname)

def write_random_data_for_whole_folder(folder, num, prefix="enhanced", suffix="", file_filter="",
                                       outputfolder=ENHANCED, min_supp=2, max_supp=0):
    """
    Generates a text file for each data file in the :param folder.
    The created file name will get the defined :param prefix and :param suffix.
    The alphabet is generated from each file

    :param folder: folder to generate the files from
    :param prefix: prefix added to the file name of each resulting file
    :param suffix: suffix added to the file name of each resulting file
    :param num: number of words per match
    :param filter: include only filenames that include this term
    :return:
    """
    min_support=min_supp
    max_support=max_supp
    matching_files = []
    if not folder.endswith("/"):
        folder = folder + "/"
    for filename in os.listdir(folder):
        if file_filter in filename or file_filter == "":
            matching_files.append(filename)

    count_files = len(matching_files)
    print("Total files with filter " + file_filter + ":\t" + str(count_files))

    for i, filename in enumerate(matching_files):
        print("\t".join(["Enhancing: ", filename]))
        name = "-".join(filter(lambda x: len(x) > 0, [prefix, filename, suffix]))
        write_random_data(name, folder + filename, num, outputfolder, min_supp=min_support, max_supp=max_support)

        print("\t".join(["Done:", str(round(i / count_files, 4)), filename]))
