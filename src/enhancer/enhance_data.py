import string
import re
from random import randint, shuffle
from parser.single_set import features_from_file
from transducer.string_alignment import align
from itertools import product
from transducer.string_alignment import GAP_CHAR
from enhancer.util import find
from project_base import PLACEHOLDER_ENHANCER
import numpy as np

PLACEHOLDER = PLACEHOLDER_ENHANCER

number_r_w = 5


def imagine_word(similar_characters_lemma, lemma, similar_characters_flex, flex, lang_model, number_words=number_r_w):
    """
    Imagines words (a lemmiddle-high-german-train-lowma and a flexation) according to the giving lemma, flex and position of similar characters.

    :param number_words:
    :param lang_model:
    :param similar_characters_flex:
    :param similar_characters_lemma: position of similar characters in the lemma
    :param lemma: Lemma to be used to generate an imagined Lemma
    :param flex: Flex to be used to generate an imagined Flexation.
    :return:
    """
    words = []
    for n in range(number_words):
        imagined_word_lemma = ""
        imagined_word_flex = ""

        fill_characters_dict = dict()

        used_patterns = []

        for c in range(len(lemma)):
            if not lemma[c] == GAP_CHAR:
                if c in similar_characters_lemma:
                    imagined_word_lemma += lemma[c]
                else:
                    imagined_word_lemma += PLACEHOLDER

        fill_characters, imagined_word_lemma = replace_placeholders_by_lang_model(fill_characters_dict,
                                                                                  imagined_word_lemma, lang_model,
                                                                                  used_patterns)

        filled_chars = 0
        for c in range(len(flex)):
            if not flex[c] == GAP_CHAR:
                fill_char = ""
                if c in similar_characters_flex:
                    imagined_word_flex += flex[c]
                else:
                    if (filled_chars < len(fill_characters)):
                        fill_char = fill_characters[filled_chars]
                        filled_chars += 1
                    else:
                        fill_char = PLACEHOLDER
                    imagined_word_flex += fill_char
        # print("\t".join(["Lemma", lemma, "Flex", flex, "Imagined Lemma", imagined_word_lemma, "Imagined Flex", imagined_word_flex, ",".join(used_patterns)]))

        if (imagined_word_flex.count(PLACEHOLDER)) > 0:
            fill_characters_flex, imagined_word_flex = replace_placeholders_by_lang_model(fill_characters_dict,
                                                                                          imagined_word_flex,
                                                                                          lang_model,
                                                                                          used_patterns)

        words.append([imagined_word_lemma, imagined_word_flex])

    return words


def replace_placeholders_by_lang_model(fill_characters_dict, imagined_word_lemma, lang_model, used_patterns):
    change = False
    while PLACEHOLDER in imagined_word_lemma:
        l = len(imagined_word_lemma)
        index = 0
        breadth = l
        if change:
            change = False
            while index < l and breadth > 0 and PLACEHOLDER in imagined_word_lemma and not change:
                while index + breadth <= l and PLACEHOLDER in imagined_word_lemma and not change:
                    current_part = imagined_word_lemma[index:(index + breadth)]
                    if current_part in lang_model:
                        fill_char = char_to_insert(imagined_word_lemma[index:(index + breadth)], lang_model)
                        pos = current_part.find(PLACEHOLDER)
                        used_patterns.append(current_part)
                        fill_characters_dict[pos + index] = fill_char
                        imagined_word_lemma = imagined_word_lemma[:(index + pos)] + fill_char + imagined_word_lemma[
                                                                                                (index + pos + 1):]
                        change = True
                    index += 1
                breadth -= 1
                index = 0
        if not change and PLACEHOLDER in imagined_word_lemma:  # if nothing changed and there are still placeholders, i.e. no n-gram fit anymore
            fill_char = char_to_insert(PLACEHOLDER, lang_model)
            candidates = find(imagined_word_lemma, PLACEHOLDER)
            shuffle(candidates)
            pos = candidates[0]
            used_patterns.append(PLACEHOLDER)
            fill_characters_dict[pos] = fill_char
            imagined_word_lemma = imagined_word_lemma[:pos] + fill_char + imagined_word_lemma[
                                                                          (pos + 1):]
            change = True

    fill_characters = []
    for i in sorted(fill_characters_dict.keys()):
        fill_characters.append(fill_characters_dict[i])
    return fill_characters, imagined_word_lemma


def char_to_insert(string, lang_model):
    keys = lang_model[string][0]
    occurences = lang_model[string][1]
    probs = normalize(occurences)
    picks = np.random.choice(keys, 1, p=probs)
    char = picks[0]
    return char


def normalize(lst):
    if sum(lst) > 0:
        return [float(i) / sum(lst) for i in lst]
    else:
        return 0


def get_random_words_by_file(file, num, lang_model, min_supp=2, max_supp=0):
    """
    Checks per wordpair with identical features where in the alignment of the wordpair the letters are identical.
    Afterwards, it generates :param num words with that same letters at the same positions, and letters from the
    respective :param alphabet in the other positions.
    The length of the random words is equal to length of the longer word.

    :param file: file to generate words from
    :param num: number of random words to be generated per word in file
    :param alph: alphabet of words to be used to generate random words
    :return: list of words (lemma, flex, features, vectors)
    """
    all_data, description = features_from_file(file, None)
    return get_random_words(all_data, num, lang_model, min_supp=min_supp, max_supp=max_supp, filename=file)


def get_random_words(all_data, num, lang_model, min_supp=2, max_supp=0, filename=""):
    """
    Checks per wordpair with identical features where in the alignment of the wordpair the letters are identical.
    Afterwards, it generates :param num words with that same letters at the same positions, and letters from the
    respective :param alphabet in the other positions.
    The length of the random words is equal to length of the longer word.

    :param all_data: Data to generate words from
    :param num: number of random words to be generated per word in file
    :param alph: alphabet of words to be used to generate random words
    :return: list of words (lemma, flex, features, vectors)
    """

    number_of_random_words = num
    imagined_data = []
    imagined_lemmata_and_features = []  # to not add

    map_of_features = dict()
    duplicates = 0
    added_words = 0
    for line in all_data:
        if not line[2] in map_of_features:
            map_of_features[line[2]] = []
        map_of_features[line[2]].append(line)

    for features in map_of_features:
        current_features = map_of_features[features]
        len_of_current_feature_list = len(current_features)
        if len_of_current_feature_list >= min_supp and (len_of_current_feature_list < max_supp or max_supp == 0):
            prodlist = list(product(current_features, current_features))
            for entry in prodlist:
                if entry[0] != entry[1]:
                    lemma1 = entry[0][0]
                    lemma2 = entry[1][0]
                    alignment_lemma = align(lemma1, lemma2)
                    similarity_lemma = []
                    pattern_lemma = ""
                    for i in range(len(alignment_lemma[0])):
                        curr_char = alignment_lemma[0][i]
                        if curr_char == alignment_lemma[1][i] and curr_char != GAP_CHAR:
                            similarity_lemma.append(i)
                            pattern_lemma += curr_char
                        elif curr_char != GAP_CHAR:
                            pattern_lemma += "."

                    pattern_lemma = "." + pattern_lemma + "."
                    while (pattern_lemma.startswith(".")):  # delete leading and trailing .s
                        pattern_lemma = pattern_lemma[1:]
                    while (pattern_lemma.endswith(".")):
                        pattern_lemma = pattern_lemma[:-1]
                    pattern_lemma = ".*" + pattern_lemma + ".*"  # and replace them with one .*

                    flex1 = entry[0][1]
                    flex2 = entry[1][1]
                    alignment_flex = align(flex1, flex2)

                    similarity_flex = []
                    pattern_flex = ""
                    for i in range(len(alignment_flex[0])):
                        curr_char = alignment_flex[0][i]
                        if curr_char == alignment_flex[1][i] and curr_char != GAP_CHAR:
                            similarity_flex.append(i)
                            pattern_flex += curr_char
                        elif curr_char != GAP_CHAR:
                            pattern_flex += "."
                    # print("\t".join([flex1, flex2, alignment_flex[0], alignment_flex[1], pattern_flex])) # for debugging

                    pattern_flex = "." + pattern_flex + "."
                    while (pattern_flex.startswith(".")):
                        pattern_flex = pattern_flex[1:]
                    while (pattern_flex.endswith(".")):
                        pattern_flex = pattern_flex[:-1]

                    pattern_flex = ".*" + pattern_flex + ".*"

                    p_lemma = re.compile(pattern_lemma)
                    occurences_pattern_lemma = 0
                    p_flex = re.compile(pattern_flex)
                    occurences_pattern_flex = 0
                    for entry2 in current_features:
                        match_lemma = p_lemma.match(entry2[0])
                        match_flex = p_flex.match(entry2[1])
                        if match_lemma:
                            occurences_pattern_lemma += 1
                        if match_flex:
                            occurences_pattern_flex += 1
                    if occurences_pattern_flex >= min_supp and occurences_pattern_lemma >= min_supp:
                        imagined_words = (
                            imagine_word(similarity_lemma, alignment_lemma[0], similarity_flex, alignment_flex[0],
                                         lang_model,
                                         num))
                        for i in range(min(number_of_random_words,
                                           len(imagined_words))):  # appends all imagined words with features
                            imagined_lemma = imagined_words[i][0]
                            lemmata = [item[0] for item in current_features]
                            if not imagined_lemma in lemmata and not [imagined_lemma,
                                                                      entry[0][2]] in imagined_lemmata_and_features: # no duplicate lemmata+features in imagined data at all
                                imagined_lemmata_and_features.append([imagined_lemma, entry[0][2]])
                                imagined_data.append(
                                    [imagined_lemma, imagined_words[i][1], entry[0][2], lemma1, lemma2, flex1, flex2])
                                added_words += 1
                            else:
                                duplicates += 1
                    # print("\t".join([lemma1, pattern_lemma, str(occurences_pattern_lemma),";", flex1, pattern_flex, str(occurences_pattern_flex)]))

    imagined_data.extend(all_data)
    print("\t".join([filename, "Words added: ", str(added_words), "Duplicates not added:", str(duplicates)]))
    return imagined_data
