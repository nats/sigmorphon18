from parser.single_set import features_from_file
from project_base import PLACEHOLDER_ENHANCER
from itertools import islice, tee

PLACEHOLDER_FOR_INSERT = PLACEHOLDER_ENHANCER

def levenshtein(s, t):
    """
    Computes the Levenshtein-Distance between two string.
    From Wikipedia article on Levenshtein Distance; Iterative with two matrix rows.
    Christopher P. Matthews
    christophermatthews1985@gmail.com
    Sacramento, CA, USA

    :param s: One string
    :param t: Other string
    :return: Levenshtein-Distance between the two strings
    """
    ''' From Wikipedia article; Iterative with two matrix rows. '''
    if s == t:
        return 0
    elif len(s) == 0:
        return len(t)
    elif len(t) == 0:
        return len(s)
    v0 = [None] * (len(t) + 1)
    v1 = [None] * (len(t) + 1)
    for i in range(len(v0)):
        v0[i] = i
    for i in range(len(s)):
        v1[0] = i + 1
        for j in range(len(t)):
            cost = 0 if s[i] == t[j] else 1
            v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
        for j in range(len(v0)):
            v0[j] = v1[j]

    return v1[len(t)]


def get_ngrams(string, n):
    return zip(*(islice(seq, index, None) for index, seq in enumerate(tee(string, n))))


def get_lang_model(filename, max_n=5):  # max_n = max length of n-gram
    all_words = []
    data = features_from_file(filename)

    map_of_ngrams = dict()

    for row in data[0]:
        all_words.append(row[0])
        all_words.append(row[1])
        for word in all_words:
            word = word.replace(PLACEHOLDER_ENHANCER, "?")
            for n in range(2, max_n):
                els = get_ngrams(word, n)
                for el in els:
                    el = "".join(el)
                    if not el in map_of_ngrams:
                        map_of_ngrams[el] = 1
                    else:
                        map_of_ngrams[el] = map_of_ngrams[el] + 1
            for el in word:
                el = "".join(el)
                if not el in map_of_ngrams:
                    map_of_ngrams[el] = 1
                else:
                    map_of_ngrams[el] = map_of_ngrams[el] + 1

    map_of_inserts = dict()

    for key in sorted(map_of_ngrams, key=map_of_ngrams.get, reverse=True):
        for i in range(len(key)):
            t = key[:i] + PLACEHOLDER_FOR_INSERT + key[i + 1:]
            if not t in map_of_inserts:
                map_of_inserts[t] = [[key[i]],[map_of_ngrams[key]]]  # TODO add probability

                # map_of_insterts[t] = [[key[i]], [prob]] # add the key and its probability
                # instead: import numpy as np, add the list of possible inserts and its probability
            else:
                map_of_inserts[t][0].append(key[i])  # TODO add probability
                map_of_inserts[t][1].append(map_of_ngrams[key])
                # map_of_insterts[t][0].extend([key[i]]) # add the key
                # map_of_inserts[t][1].extend([prob]) # add its probability
                # instead: add the list of possible inserts and its probability

    return map_of_inserts


def find(s, ch):
    return [i for i, ltr in enumerate(s) if ltr == ch]
