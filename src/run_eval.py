from project_base import LANGUAGES
from evalm import main as evalm
import os

FOLDER = "/home/marcel/git/conll2018/task1/all"
VOLUMES = ["low", "medium", "high"]

result = []
for lang in LANGUAGES:
    for vol in VOLUMES:
        gold = os.path.join(FOLDER, lang + "-dev")
        guess = os.path.join(FOLDER, lang + "-" + vol + "-out")
        if os.path.isfile(gold) and os.path.isfile(guess):
            result.append(evalm(gold, guess, 1, message=lang))

