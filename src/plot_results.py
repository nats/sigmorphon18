import argparse
import numpy as np
import matplotlib.pyplot as plt

from project_base import output_path

IND_ACC = 2
IND_LEV = 4
IND_OUT = 8


def parse_log(file):
    languages = []
    acc_dev = []
    lev_dev = []
    acc_train = []
    lev_train = []
    with open(file) as fp:
        for line in fp:
            tokens = line.split("\t")
            acc = float(tokens[IND_ACC])
            lev = float(tokens[IND_LEV])
            output = tokens[IND_OUT]
            dev_idx = output.find("-dev-")
            start_idx = output.find("result-") + 7
            if dev_idx > 0:
                lang = output[start_idx:dev_idx]
                # languages.append(lang)
                acc_dev.append(acc)
                lev_dev.append(lev)
            else:
                train_idx = output.find("-train-")
                lang = output[start_idx:train_idx]
                languages.append(lang)
                acc_train.append(acc)
                lev_train.append(lev)
    return languages, np.array(acc_train), np.array(lev_train), np.array(acc_dev), np.array(lev_dev)


def plot(l, t, b, title):
    plt.bar(l, b)
    plt.bar(l, t-b, bottom=b)
    plt.title(title)
    plt.xticks(l, rotation='vertical')
    plt.show()


def main(logfile):
    log_path = output_path('evaluation', file=logfile)

    languages, acc_train, lev_train, acc_dev, lev_dev = parse_log(log_path)
    plot(languages, acc_train, acc_dev, "acc")
    plot(languages, lev_dev, lev_train, "lev")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extracts results from log file and plots them')
    parser.add_argument('-l', '--logfile', dest='logfile', default='results_log.txt')

    args = parser.parse_args()

    main(args.logfile)