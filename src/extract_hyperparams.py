#! /usr/bin/env python3

import os
import re
import sys

ACC_POS = 2
LEV_POS = 5
PARAMS_POS = 6

def parse(folder, size="medium"):
	languages = []
	results = []
	lang_folders = os.listdir(folder)
	for lang in lang_folders:
		curr_path = os.path.join(folder, lang, size)
		lang_acc_log = 	os.path.join(curr_path, "best_levenshtein_parameters_search.log")	
		if os.path.isfile(lang_acc_log):
			with open(lang_acc_log) as f:
				data = f.readlines()
			lastline = data[-1]
			info = lastline.split("\t")
			params = info[PARAMS_POS][1:len(info[PARAMS_POS])-2]
			params = re.split(", | ", params)
			cleaned_params = map(lambda p: p.strip(":'\""), params)
			#print(params)
			results.append("\t".join([lang, "\t".join(info[ACC_POS-1:LEV_POS]), "\t".join(cleaned_params)]))
	with open("parse_results.txt", "w+") as result_file:
		result_file.write("\n".join(sorted(results)))  
	print("done")

PARAMS_START = 5

def get_best_params_for_lang(params_list_filename, language):
	with open(params_list_filename) as f:
		for line in f:
			if line.startswith(language):
				array = line.strip().split("\t")[5:]
				i = iter(array)
				result = {}
				for k, v in dict(zip(i, i)).items():
					val = v
					try:
						val = int(v)
					except ValueError as ve:
						if v == 'True':
							val = True
						elif v == 'False':
							val = False
					result[k.replace("-", "_")] = val
					
				#print(result)
				return result
	return None


if __name__ == '__main__':
	parse(sys.argv[1])
